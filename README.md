# VLC-C播放器

#### 介绍
控制方式通过http协议发送指令控制播放、暂停、控制电脑音量、控制播放器音量、多播放器联动播放、关电脑、重启电脑、切换视频、切换其他应用(隐藏播放器后切换到指定程序)等。
内置浏览器，自带页面通过浏览器访问播放器首页、导入资源（资源类型:视频、图片、音频、可执行文件、互动程序、待机画面）、设置、退出等。提供心跳上报、本地导入资源上报、监控、授权试用(过期后屏幕输出过期等字样)、在线发布资源、资源分片下载、断点续传、在线更新，前提是要搭建一个服务器跟播放器对接，播放器已经有接口。

#### 软件架构
软件架构说明:

Open-JDK-11<BR>spring-boot-2.6.10<BR><BR>javafx<BR>swing<BR>vlcj-4.7.1

#### 安装教程

1. 官网下载Vlc播放器,要安装完播放器后才能运行.官网下载:https://www.videolan.org;
2. 目录中data文件夹里面的文件是播放器重要文件,测试环境(开发)将文件夹复制到跟项目同一级目录(觉得麻烦可自行修改读取路径),生产环境也和jar包放在同一目录,读取不到文件会运行不了。


#### 使用说明

http协议控制<BR>
地址:HTTP://127.0.0.1:19220/terminal<BR>
json数据格式<BR>
POST请求<BR>
指令：<BR>
播放：{"type":"PLAYER_PLAY","command":"V0001"}<BR>
暂停：{"type":"PLAYER_PLAY","command":"V0002"}<BR>
停止：{"type":"PLAYER_PLAY","command":"V0003"}<BR>
重播：{"type":"PLAYER_PLAY","command":"V0008"}<BR>
增加音量：{"type":"PLAYER_PLAY","command":"SY002"}<BR>
增加音量：{"type":"PLAYER_PLAY","command":"SY003"}<BR>

获取播放列表{"type":"PLAYER_PLAY","command":"PL001"}<BR>
更多指令查看 指令常量CommandConst.java<BR>


socket控制协议:<BR>
未提供，自行添加


#### 已知缺陷
1. 不支持8K视频
2. 接入音响后播放器音量低于20左右时低音声道不明显
3. 联动播放，同步稍微有延迟并且不固定，最大不会大于1s、概率低，优化为socket应该会有改善
4. 画面无法缩放，受vlc官方依赖影响，得等官方优化。

#### 参与贡献

1. 作者CCC


### 浏览图

播放器

![image](src/main/resources/static/img/VLC播放器.png)<BR>

首页

![image](src/main/resources/static/img/首页.png)<BR>

设置

![image](src/main/resources/static/img/设置.png)<BR>

资源

![image](src/main/resources/static/img/资源.png)<BR>

支持的资源类型

![image](src/main/resources/static/img/支持的资源类型.png)<BR>
