package com.frontop.terminal.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>
 *
 * </p>
 *
 * @author Frontop
 * @since :2022-06-22
 **/
@Getter
@AllArgsConstructor
public enum ResultCodeEnum {

    /**
     * 成功
     */
    SUCCEED("true", 200),
    BAD_REQUEST("无效请求, 请求参数有误", 400),
    UNAUTHORIZED("未获得授权", 401),
    NOT_FOUND("找不到数据", 404),
    FORBIDDEN("没有访问权限", 403),
    INTERNAL_SERVER_ERROR("内部服务器错误", 500);


    private final String msg;
    private final Integer code;
}
