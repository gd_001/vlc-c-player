package com.frontop.terminal.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * <p>
 * 请求头枚举
 * </P>
 *
 * @author :CCC
 * @since :2021-05-21 17:05:00
 **/
@RequiredArgsConstructor
@Getter
public enum HeaderEnum {

    /**
     * 认证类型
     */
    AUTH_TYPE("Auth-Type", "TENANT"),

    /**
     * 终端凭证
     */
    TERMINAL_TOKEN("Terminal-Token", "");

    private final String key;

    private final String value;
}
