package com.frontop.terminal.constant;

/**
 * <p>
 * 下载状态常量
 * </p>
 *
 * @author :CCC
 * @since :2021-06-10
 **/
public class DownloadConst {

    /**
     * 等待中
     */
    public static final Integer NOT_START = 0;

    /**
     * 进行中
     */
    public static final Integer IN_PROGRESS = 1;

    /**
     * 完成
     */
    public static final Integer ACCOMPLISH = 2;
}
