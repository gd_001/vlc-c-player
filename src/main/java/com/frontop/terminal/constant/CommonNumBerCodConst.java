package com.frontop.terminal.constant;

/**
 * <p>
 * 通用数字code
 * </p>
 *
 * @author :CCC
 * @since :2021-08-11
 **/
public class CommonNumBerCodConst {

    public static Integer ZERO = 0;
    public static Integer ON = 1;
    public static Integer TWO = 2;
    public static Integer THREE = 3;
    public static Integer FOUR = 4;
    public static Integer FIVE = 5;
    public static Integer SIX = 6;
    public static Integer SEVEN = 7;
    public static Integer EIGHT = 8;
    public static Integer NINE = 9;
}
