package com.frontop.terminal.constant;

/**
 * <p>
 * 服务器接口地址
 * </p>
 *
 * @author :CCC
 * @since :2021-05-24 17:17:00
 **/
public class InterfaceConst {

    /**
     * 本机首页
     */
    public static String LOCAL_TERMINAL_HOMEPAGE = "http://127.0.0.1:19220/terminal/index";

    /**
     * 本机配置页面
     */
    public static String LOCAL_TERMINAL_CONFIG = "http://127.0.0.1:19220/terminal-config";

    private static final String ROOT = "/t/ccm/terminal/agent/";

    /**
     * 发送心跳接口
     */
    public static String SEND_HEARTBEAT = ROOT + "sendHeartbeat";

    /**
     * 测试连接接口
     */
    public static String TEST = ROOT + "test";

    /**
     * 获取已发布资源列表
     */
    public static String GET_PUBLISH_RESOURCE_LIST = ROOT + "getPublishResourceList";

    /**
     * 删除资源通知中控系统同步删除
     */
    public static String DELETE_RESOURCE = ROOT + "deleteResource";

    /**
     * 修改资源名称
     */
    public static String UPDATE_RESOURCE_NAME = ROOT + "updateResourceName";


    /**
     * 通知中控同步导入新资源
     */
    public static String ADD_RESOURCE = ROOT + "addResource";

    /**
     * 通知中控移除资源成功
     */
    public static String REMOVE_RESOURCE_SUCCESS = ROOT + "removeResourceSuccess";

    /**
     * 终端下载资源成功后通知中控服务器修改资源状态接口
     */
    public static String DOWNLOAD_RESOURCE_SUCCESS = ROOT + "downloadResourceSuccess";

    /**
     * 通知服务器同步导入互动程序
     */
    public static String ADD_INTERACTIVE_PROGRAM = ROOT + "addInteractiveProgram";

    /**
     * 获取中控存储文件(用于下载文件)
     */
    public static String GET_FILE_LIST = ROOT + "getFileList";

    /**
     * 分片下载资源接口
     */
    public static String TERMINAL_AGENT_RESOURCE_PART_DOWNLOAD = ROOT + "terminalAgentResourcePartDownload/";

    /**
     * 获取服务器配置
     */
    public static String GET_TERMINAL_CONFIG = ROOT + "getTerminalConfig";

    /**
     * 同步修改终端配置
     */
    public static String UPDATE_TERMINAL_CONFIG = ROOT + "updateTerminalConfig";

}
