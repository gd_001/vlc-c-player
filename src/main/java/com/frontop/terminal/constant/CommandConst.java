package com.frontop.terminal.constant;

/**
 * <p>
 * 具体命令
 * </p>
 *
 * @author :CCC
 * @since :2021-05-26
 **/
public class CommandConst {

    /**
     * cmd类型命令-关机
     */
    public static final String SHUTDOWN = "A0001";


    /**
     * cmd类型命令-重启
     */
    public static final String RESTART = "A0002";

    /**
     * 播放列表类型命令-更新数据
     */
    public static final String REFRESH = "B0001";

    /**
     * 视频播放类型命令-播放
     */
    public static final String PLAY = "V0001";

    /**
     * 视频播放类型命令-暂停
     */
    public static final String PLAY_PAUSE = "V0002";

    /**
     * 视频播放类型命令-停止
     */
    public static final String PLAY_STOP = "V0003";

    /**
     * 视频播放类型命令-上一个
     */
    public static final String PLAY_LAST = "V0004";

    /**
     * 视频播放类型命令-下一个
     */
    public static final String PLAY_NEXT = "V0005";

    /**
     * 视频播放类型命令-快进
     */
    public static final String PLAY_NEXT_FRAME = "V0006";

    /**
     * 视频播放类型命令-后退
     */
    public static final String PLAY_UP_FRAME = "V0007";

    /**
     * 视频播放类型命令-重播 =停止+播放
     */
    public static final String PLAY_REBROADCAST = "V0008";

    /**
     * 音量-静音
     */
    public static final String AUDIO_MUTE = "Y0001";

    /**
     * 音量-取消静音
     */
    public static final String AUDIO_CANCEL_MUTE = "Y0002";

    /**
     * 音量-获取音量. 范围0~200
     */
    public static final String AUDIO_GET_VOLUME = "Y0003";

    /**
     * 音量-获取是否被静音
     */
    public static final String AUDIO_GET_MUTE = "Y0004";

    /**
     * 音量-增加音量 +10 范围0-200
     */
    public static final String AUDIO_AMPLIFY = "Y0005";

    /**
     * 音量-减少音量 -10 范围0-200
     */
    public static final String AUDIO_DECREASE = "Y0006";

    /**
     * 系统音量-静音/取消静音 todo 音量用错单词，下版本更改
     */
    public static final String SYSTEM_AUDIO_MUTE_ON_OFF = "SY001";

    /**
     * 系统音量-增加音量
     */
    public static final String SYSTEM_AUDIO_AMPLIFY = "SY002";

    /**
     * 系统音量-减少音量
     */
    public static final String SYSTEM_AUDIO_DECREASE = "SY003";

    /**
     * 播放器当前是否在播放资源
     */
    public static final String PLAYER_STATUS_IS_PLAY = "PT001";

    /**
     * 系统授权许可类型-未激活
     */
    public static final String LICENSE_UNACTIVATED = "LU001";

    /**
     * 系统授权许可类型-激活-关闭
     */
    public static final String LICENSE_ACTIVATED = "LU002";


}
