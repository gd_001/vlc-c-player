package com.frontop.terminal.constant;

/**
 * <p>
 * 播放列表状态
 * </p>
 *
 * @author :CCC
 * @since :2021-06-15 09:40:00
 **/
public class PlayStatusConst {
    /**
     * 等待中
     */
    public static final Integer WAITING = 0;

    /**
     * 进行中
     */
    public static final Integer UNDER_WAY = 1;

    /**
     * 结束
     */
    public static final Integer FINISH = 2;
    /**
     * 待机资源TYPE
     */
    public static final Integer TYPE_STANDBY_RESOURCE = 1;
    /**
     * 播放模式 - 列表循环
     */
    public static Integer PLAYP_PATTERN_LIST = 1;
    /**
     * 播放模式 - 单个循环
     */
    public static Integer PLAYP_PATTERN_ON = 2;
    /**
     * 播放模式 - 播放完当前视频即结束
     */
    public static Integer PLAYP_CURRENT_TO_END = 3;
}
