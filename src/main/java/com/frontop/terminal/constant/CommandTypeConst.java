package com.frontop.terminal.constant;

/**
 * <p>
 * 命令类型常量
 * </p>
 *
 * @author :CCC
 * @since :2012-01-01 00:47:00
 * 具体命令查看{@link CommandConst}
 **/
public class CommandTypeConst {

    /**
     * 播放资源
     */
    public static final String PLAYER_PLAY = "PLAYER_PLAY";

    /**
     * 下载相关命令
     */
    public static final String DOWNLOAD = "DOWNLOAD";

    /**
     * CMD相关命令
     */
    public static final String CMD = "CMD";

    /**
     * 播放列表相关
     */
    public static final String PLAYER_PLAY_LIST = "PLAYER_PLAY_LIST";

    /**
     * 播放器音量
     */
    public static final String PLAYER_AUDIO = "PLAYER_AUDIO";

    /**
     * 系统音量
     */
    public static final String SYSTEM_AUDIO = "SYSTEM_AUDIO";

    /**
     * 检测更新程序
     */
    public static final String CHECK_UPDATE = "CHECK_UPDATE";

    /**
     * 播放器状态
     */
    public static final String PLAYER_STATUS = "PLAYER_STATUS";

    /**
     * 移除播放列表资源
     */
    public static final String PLAYER_REMOVE_RESOURCE = "PLAYER_REMOVE_RESOURCE";

    /**
     * 发布播放列表资源
     */
    public static final String PLAYER_PUBLISH_RESOURCE = "PLAYER_PUBLISH_RESOURCE";

    /**
     * 发布互动程序资源
     */
    public static final String PROGRAM_PUBLISH_RESOURCE = "PROGRAM_PUBLISH_RESOURCE";

    /**
     * 修改资源名称
     */
    public static final String PLAYER_UPDATE_RESOURCE_NAME = "PLAYER_UPDATE_RESOURCE_NAME";

    /**
     * 移除互动程序资源
     */
    public static final String PROGRAM_REMOVE_RESOURCE = "PROGRAM_REMOVE_RESOURCE";

    /**
     * 修改互动程序名称
     */
    public static final String PROGRAM_UPDATE_NAME = "PROGRAM_UPDATE_NAME";

    /**
     * 系统授权许可
     */
    public static final String SYSTEM_LICENSE = "SYSTEM_LICENSE";


}
