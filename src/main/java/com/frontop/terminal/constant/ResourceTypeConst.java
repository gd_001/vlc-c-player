package com.frontop.terminal.constant;

/**
 * <p>
 * 资源类型
 * </p>
 *
 * @author :CCC
 * @since :2021-07-22
 **/
public class ResourceTypeConst {
    /**
     * 视频
     */
    public static final Integer VIDEO = 1;

    /**
     * 图片
     */
    public static final Integer IMG = 2;

    /**
     * 音频
     */
    public static final Integer AUDIO = 3;

    /**
     * 可执行文件
     */
    public static final Integer EXE = 4;

    /**
     * 其他
     */
    public static final Integer RESTS = 5;

    /**
     * 互动程序
     */
    public static final Integer INTERACTION = 6;

    /**
     * 待机
     */
    public static final Integer STANDBY = 7;

    public static Integer converttoservercode(int code) {
        switch (code) {
            case 1:
                return 1;
            case 2:
                return 3;
            case 3:
                return 2;
            case 4:
            case 6:
                return 4;
            case 5:
                return 5;
            case 7:
                return 6;
            default:
                return 99;
        }
    }

}
