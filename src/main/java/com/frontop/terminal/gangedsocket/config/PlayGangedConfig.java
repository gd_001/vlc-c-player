package com.frontop.terminal.gangedsocket.config;

import com.frontop.terminal.util.SystemMonitoringUtil;

/**
 * <p>
 * socket属性配置
 * </p>
 *
 * @author :CCC
 * @since :2021-10-13
 **/
public class PlayGangedConfig {
    public final static Integer PORT = 10038;
    public final static Integer CONNECTION_MAX = 100;
    public final static String HOST_ADDRESS = SystemMonitoringUtil.getTerminal().getIp() + ":" + PlayGangedConfig.PORT;

}
