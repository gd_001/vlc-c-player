package com.frontop.terminal.gangedsocket.service.impl;

import cn.hutool.core.thread.ThreadUtil;
import com.frontop.terminal.cmd.AbstractCmdCommand;
import com.frontop.terminal.constant.CommandConst;
import com.frontop.terminal.constant.CommonNumBerCodConst;
import com.frontop.terminal.entity.Message;
import com.frontop.terminal.entity.Program;
import com.frontop.terminal.gangedsocket.service.ICommandSerivce;
import com.frontop.terminal.util.ReadDataUtil;
import com.frontop.terminal.util.TerminalApplictionContextUtil;
import com.frontop.terminal.vlcjplayer.VlcjPlayer;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 处理CMD命令实现类
 * </p>
 *
 * @author :CCC
 * @since :2021-05-26 14:21:00
 **/
@Slf4j
public class CmdCommandServiceImpl implements ICommandSerivce {

    @Override
    public void executeWork(Message message) {
        AbstractCmdCommand abstractCmdCommand = AbstractCmdCommand.getCmdCommandFactory();

        switch (message.getCommand()) {
            case CommandConst.SHUTDOWN:
                abstractCmdCommand.shutdown();
                break;
            case CommandConst.RESTART:
                abstractCmdCommand.restart();
                break;
            default:

                try {
                    //尝试将具体命令转为int，成功视作正常命令
                    String exeId = message.getCommand();
                    //读取程序列表
                    List<Program> programList = ReadDataUtil.getProgramList();
                    Program program = null;
                    if (Objects.isNull(programList) || programList.isEmpty()) {
                        System.out.println("终端代理的互动程序列表为空");
                        return;
                    }

                    for (Program p :
                            programList) {
                        if (Objects.nonNull(p.getGangedId()) && p.getGangedId().equals(exeId)) {
                            program = p;
                        }
                    }

                    if (Objects.isNull(program)) {
                        System.out.println(exeId + "在互动程序列表中不存在");
                        return;
                    }

                    //以防重复打开，先关闭程序
                    String programName = program.getLocation().substring(program.getLocation().lastIndexOf("\\") + 1);
                    log.info("启动程序前尝试关闭重复进程:[{}]", programName);
                    abstractCmdCommand.killedExe(programName);

                    ThreadUtil.sleep(600);
                    //启动
                    log.info("启动程序中,路径:[{}]", program.getLocation());
                    abstractCmdCommand.runExe(program.getLocation());
                    //切换到互动程序时隐藏播放器
                    VlcjPlayer.vlcPlayerVisible(false);
                    //记录互动程序id
                    TerminalApplictionContextUtil.RESOURCE_ID = exeId;
                    TerminalApplictionContextUtil.CURRENT_FOCUS_TASK = CommonNumBerCodConst.TWO;
                } catch (Exception e) {
                    TerminalApplictionContextUtil.CURRENT_FOCUS_TASK = CommonNumBerCodConst.ZERO;
                    //如果说出现异常可能就是类型转换错误，就认为参数不合法
                    e.printStackTrace();
                    System.out.println("参数不合法");
                }
        }

    }
}
