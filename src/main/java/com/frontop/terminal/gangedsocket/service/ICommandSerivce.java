package com.frontop.terminal.gangedsocket.service;

import com.frontop.terminal.entity.Message;

/**
 * <p>
 * 命令业务接口
 * </p>
 *
 * @author :CCC
 * @since :2021-05-26 14:12:00
 **/
public interface ICommandSerivce {

    /**
     * 执行业务
     *
     * @param message 消息对象
     * @return T
     */
    void executeWork(Message message);
}
