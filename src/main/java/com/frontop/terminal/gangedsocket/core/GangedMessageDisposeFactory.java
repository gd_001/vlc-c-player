package com.frontop.terminal.gangedsocket.core;

import com.frontop.terminal.constant.CommandTypeConst;
import com.frontop.terminal.gangedsocket.service.ICommandSerivce;
import com.frontop.terminal.gangedsocket.service.impl.CmdCommandServiceImpl;
import com.frontop.terminal.gangedsocket.service.impl.PlayServiceImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 联动消息工厂
 * </P>
 *
 * @author :CCC
 * @since :2021-05-26 16:58:00
 **/
public class GangedMessageDisposeFactory {

    private static final Map<String, ICommandSerivce> MESSAGE_MAP;

    static {
        MESSAGE_MAP = new HashMap<>();
        // CMD类型命令
        MESSAGE_MAP.put(CommandTypeConst.CMD, new CmdCommandServiceImpl());
        //播放类型命令
        MESSAGE_MAP.put(CommandTypeConst.PLAYER_PLAY, new PlayServiceImpl());
    }

    /**
     * 获取业务处理对象
     *
     * @param key 命令
     * @return ICommandService
     */
    public static ICommandSerivce getWorkers(String key) {
        return MESSAGE_MAP.get(key);
    }
}
