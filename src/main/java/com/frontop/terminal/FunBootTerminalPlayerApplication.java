package com.frontop.terminal;

import com.frontop.terminal.component.StartTask;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author frontop
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableScheduling
public class FunBootTerminalPlayerApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(FunBootTerminalPlayerApplication.class);
        //设置应用程序是否无头且不应该实例化AWT。默认值为true，防止java图标出现。
        builder.headless(false).run(args);

        try {
            StartTask.init();
        } catch (Exception e) {
            System.err.println(("[全局异常]程序出现未捕获异常:" + e.getMessage()));
            e.printStackTrace();
        }
    }

}
