package com.frontop.terminal.cmd;

/**
 * <p>
 * windows命令
 * </p>
 *
 * @author :CCC
 * @since :2021-05-26
 **/
@SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
public class WindowsCmdCommand extends AbstractCmdCommand {
    @Override
    public void shutdown() {
        execute("shutdown -s -f -t 5");
    }

    @Override
    public void restart() {
        execute("shutdown -r -f -t 5");
    }

    @Override
    public void shutdown(int timing) {
        execute("shutdown -s -f -t " + timing);
    }

    @Override
    public void restart(int timing) {
        execute("shutdown -r -f -t " + timing);
    }

    @Override
    public void runExe(String path) {
        execute("cmd /c start /min \"\" \"" + path + "\"");
    }

    @Override
    public void killedExe(String procedureName) {
        execute("taskkill /f /t /im " + procedureName);
    }
}
