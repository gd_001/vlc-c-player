package com.frontop.terminal.cmd;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * <p>
 * cmd命令
 * </p>
 *
 * @author :CCC
 * @since :2021-05-26 11:06:00
 **/
@SuppressWarnings("unused")
@Slf4j
public abstract class AbstractCmdCommand {

    /**
     * 获取对应操作系统cmd命令对象
     *
     * @return AbstractCmdCommand
     */
    public static AbstractCmdCommand getCmdCommandFactory() {
        boolean isWindows = System.getProperty("os.name").toLowerCase().contains("win");
        AbstractCmdCommand abstractCmdCommand;
        if (isWindows) {
            log.warn("当前是Windows环境命令");
            abstractCmdCommand = new WindowsCmdCommand();
        } else {
            log.warn("默认是Linux环境命令");
            abstractCmdCommand = new LinuxCmdCommand();
        }
        return abstractCmdCommand;
    }

    /**
     * 关机
     */
    public abstract void shutdown();

    /**
     * 重启
     */
    public abstract void restart();

    /**
     * 定时关机
     *
     * @param timing 指定秒数
     */
    public abstract void shutdown(int timing);

    /**
     * 定时重启
     *
     * @param timing 指定秒数
     */
    public abstract void restart(int timing);

    /**
     * 运行exe程序
     *
     * @param path 程序路径
     */
    public abstract void runExe(String path);

    /**
     * 杀死进程
     *
     * @param procedureName 程序名称（不是别名）
     */
    public abstract void killedExe(String procedureName);

    protected void execute(String command) {
        try {
            Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
