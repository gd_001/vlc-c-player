package com.frontop.terminal.cmd;

/**
 * <p>
 * Linux命令,create :
 * </P>
 *
 * @author :CCC
 * @since :2021-05-26
 **/
public class LinuxCmdCommand extends AbstractCmdCommand {
    @Override
    public void shutdown() {
        execute("shutdown -h now");
    }

    @Override
    public void restart() {
        execute("shutdown -r now");
    }

    @Override
    public void shutdown(int timing) {
        execute("shutdown -h " + timing);
    }

    @Override
    public void restart(int timing) {
        execute("shutdown -r " + timing);
    }

    @Override
    public void runExe(String path) {
        // todo 等待实现
    }

    @Override
    public void killedExe(String procedureName) {
        // todo 等待实现
    }
}
