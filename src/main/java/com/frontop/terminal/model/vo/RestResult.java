package com.frontop.terminal.model.vo;

import com.frontop.terminal.enums.ResultCodeEnum;
import lombok.Data;
import lombok.ToString;

/**
 * <p>
 *
 * </p>
 *
 * @author Frontop
 * @since :2022-06-22
 **/
@Data
@ToString
public class RestResult {

    private String msg;

    private Integer code;

    private Object data;

    public static RestResult success() {
        RestResult restResult = new RestResult();
        restResult.setCode(ResultCodeEnum.SUCCEED.getCode());
        restResult.setMsg(ResultCodeEnum.SUCCEED.getMsg());

        return restResult;
    }

    public static RestResult success(String msg) {
        RestResult restResult = new RestResult();
        restResult.setCode(ResultCodeEnum.SUCCEED.getCode());
        restResult.setMsg(msg);

        return restResult;
    }

    public static RestResult success(Object data) {
        RestResult restResult = new RestResult();
        restResult.setCode(ResultCodeEnum.SUCCEED.getCode());
        restResult.setMsg("true");
        restResult.setData(data);

        return restResult;
    }

    public static RestResult failed() {
        RestResult restResult = new RestResult();
        restResult.setCode(ResultCodeEnum.BAD_REQUEST.getCode());
        restResult.setMsg(ResultCodeEnum.BAD_REQUEST.getMsg());

        return restResult;
    }

    public static RestResult failed(String msg) {
        RestResult restResult = new RestResult();
        restResult.setCode(ResultCodeEnum.BAD_REQUEST.getCode());
        restResult.setMsg(msg);

        return restResult;
    }

    public static RestResult failed(ResultCodeEnum resultCodeEnum) {
        RestResult restResult = new RestResult();
        restResult.setCode(resultCodeEnum.getCode());
        restResult.setMsg(resultCodeEnum.getMsg());

        return restResult;
    }
}
