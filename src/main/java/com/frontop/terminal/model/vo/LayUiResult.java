package com.frontop.terminal.model.vo;

import lombok.Data;

/**
 * <p>
 * layui请求结果封装类
 * </p>
 *
 * @author :CCC
 * @since :2021-10-08
 **/
@Data
public class LayUiResult {
    private Integer code;
    private String msg;
    private Integer count;
    private Object data;

    public static LayUiResult success(String msg, Integer count, Object data) {
        LayUiResult layUiResult = new LayUiResult();
        layUiResult.code = 0;
        layUiResult.count = count;
        layUiResult.data = data;
        layUiResult.msg = msg;
        return layUiResult;
    }
}
