package com.frontop.terminal.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author :CCC
 * @since :2021-10-11
 **/
@Data
public class ResourceVO implements Serializable {
    private static final long serialVersionUID = 4092254191635668053L;

    private String id;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 关联资源ID
     */
    private Long fileId;

    /**
     * 开启播放联动时的关联主屏资源id
     */
    private String gangedId;

    /**
     * 状态：0等待，1播放中
     */
    private Integer status;

    /**
     * 文件存放位置
     */
    private String location;

    /**
     * 资源加入来源：0,中控服务器下载;1,手动导入
     */
    private Integer source = 0;

    /**
     * 类型：0普通资源;1,待机资源;2互动程序
     */
    private Integer type = 0;
}
