package com.frontop.terminal.model.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 终端资源BO
 * </p>
 *
 * @author Frontop
 * @since :2022-06-02
 **/
@Data
public class TerminalResourceBO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private Long resourceId;

    /**
     * "状态:1未发布,2发布中,3已发布,4撤销中,5已撤销发布"
     */
    private Integer resourceStatus;

    /**
     * 资源名称
     */
    private String resourceName;

    /**
     * 资源类型:1影片,2音频,3图片,4互动程序,5其他,6待机资源
     */
    private Integer resourceType;

    /**
     * size
     */
    private Long size;

    /**
     * 来源:1中控发布,2终端本地导入
     */
    private Integer resourceSource;

    private String mac;

    /**
     * 资源路径 记录终端本地导入的资源路径
     */
    private String resourceLocation;
}
