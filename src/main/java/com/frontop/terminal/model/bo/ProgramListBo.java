package com.frontop.terminal.model.bo;

import com.frontop.terminal.entity.Program;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 互动程序list的数据类
 *
 * @author Doumou
 * @date 2021/7/21
 */
@Data
public class ProgramListBo implements Serializable {

    private static final long serialVersionUID = -4527595531721522813L;

    /**
     * 终端的mac地址
     */
    private String mac;

    private List<Program> programs;

    public ProgramListBo(String mac, List<Program> programs) {
        this.mac = mac;
        this.programs = programs;
    }
}
