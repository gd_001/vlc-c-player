package com.frontop.terminal.model.bo;

import com.alibaba.fastjson.annotation.JSONField;
import com.frontop.terminal.entity.TerminalConfig;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 终端配置BO实体 {@link TerminalConfig}
 * </p>
 *
 * @author Rich
 * @since 2021-08-03
 */
@Data
public class TerminalConfigBo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 播放方式：1.列表轮播；2.单个循环；3.播放当前完毕
     */
    private Integer playMode;

    /**
     * 启动时是否隐藏播放器，只有在播放时才显示，停止时隐藏
     */
    private Boolean hide;

    /**
     * 打开浏览器是否全屏
     */
    private Boolean fullScreen;

    /**
     * 打开播放器后是否自动播放视频
     */
    private Boolean autoPlay;

    /**
     * 播放完顺序后，是否停留在最后一帧画面
     */
    private Boolean lastFrame;

    /**
     * 浏览器是否隐藏
     */
    private Boolean browserHide;

    /**
     * 终端的mac
     */
    @JSONField(name = "terminalMac")
    private String mac;
}
