package com.frontop.terminal.model.bo;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 终端版本信息
 * </p>
 *
 * @author Rich
 * @since 2021-08-31
 */
@Data
public class TerminalVersionBo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 更新日志
     */
    private String log;

    /**
     * 版本号
     */
    private String version;
}
