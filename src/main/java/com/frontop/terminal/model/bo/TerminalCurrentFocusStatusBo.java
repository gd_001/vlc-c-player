package com.frontop.terminal.model.bo;

import lombok.Data;

/**
 * <p>
 * 终端当前焦点状态bo
 * </P>
 *
 * @author :CCC
 * @since :2021-08-11 15:00:00
 **/
@Data
public class TerminalCurrentFocusStatusBo {

    /**
     * 当前程序切换到播放器或者互动程序
     * 1 播放器,2 互动程序, 0 待机
     */
    private Integer currentFocusTask;
    /**
     * 当前资源id，可能是媒体id也可能是互动程序id
     */
    private String resourceId;

    public TerminalCurrentFocusStatusBo(Integer currentFocusTask, String resourceId) {
        this.currentFocusTask = currentFocusTask;
        this.resourceId = resourceId;
    }
}
