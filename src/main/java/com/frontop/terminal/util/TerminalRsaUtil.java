package com.frontop.terminal.util;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

/**
 * @author Doumou
 * @date 2021/5/21
 */
@Component
public class TerminalRsaUtil {

    /**
     * 终端与中控互信公钥
     */
    private static final String TERMINAL_RSA_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC60yZzs1/PC0lisoW+RmXslzEMefKEOZdIccR3HwINzkrnCSqw7WIPIYmuZC4cuMsUYhvS1CAPqyICn/RxTlSt/vutY2EVkX9ua+1XEp8PTDL4n5+dpwvrAJqRKF/21ALSEFasRsvr6AbbIH+gl9sUFq5HC6D3c5FH5hZpOqB1RQIDAQAB";
    private static final RSA TERMINAL_RSA;
    private static String tenant;
    private static String token;

    static {
        TERMINAL_RSA = new RSA(null, TERMINAL_RSA_PUBLIC_KEY);
    }

    public static String getTenant() {
        return tenant;
    }

    public static String getToken() {
        return token;
    }


    /**
     * 此方法用于解析中控的认证token,true为互信;
     *
     * @param terminalToken terminalToken
     * @return Boolean
     */
    public static Boolean terminalTokenResolver(String terminalToken) {
        if (Strings.isEmpty(terminalToken)) {
            return false;
        }
        String decryptStr = TERMINAL_RSA.decryptStr(terminalToken, KeyType.PublicKey);
        return tenant.equals(decryptStr);
    }

}
