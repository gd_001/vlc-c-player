package com.frontop.terminal.util;

/**
 * <p>
 * 终端程序上下文
 * </P>
 *
 * @author :CCC
 * @since :2021-08-10 15:27:00
 **/
public class TerminalApplictionContextUtil {

    /**
     * 当前程序切换到播放器或者互动程序
     * 1 播放器,2 互动程序, 0 待机
     */
    public static volatile Integer CURRENT_FOCUS_TASK = 0;

    /**
     * 当前资源id，可能是媒体id也可能是互动程序id
     */
    public static volatile String RESOURCE_ID;

}
