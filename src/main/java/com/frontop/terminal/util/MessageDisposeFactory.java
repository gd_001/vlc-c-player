package com.frontop.terminal.util;

import com.frontop.terminal.constant.CommandTypeConst;
import com.frontop.terminal.service.ICommandSerivce;
import com.frontop.terminal.service.impl.AudioServiceImpl;
import com.frontop.terminal.service.impl.CmdCommandServiceImpl;
import com.frontop.terminal.service.impl.LicenseServiceImpl;
import com.frontop.terminal.service.impl.PlayListServiceImpl;
import com.frontop.terminal.service.impl.PlayServiceImpl;
import com.frontop.terminal.service.impl.PlayerPublishResourceServiceImpl;
import com.frontop.terminal.service.impl.PlayerRemoveResourceServiceImpl;
import com.frontop.terminal.service.impl.PlayerStatusServiceImpl;
import com.frontop.terminal.service.impl.PlayerUpdateResourceNameServiceImpl;
import com.frontop.terminal.service.impl.ProcedurePublishResourceServiceImpl;
import com.frontop.terminal.service.impl.ProcedureUpdateServiceImpl;
import com.frontop.terminal.service.impl.ProgramRemoveServiceImpl;
import com.frontop.terminal.service.impl.ProgramUpdateNameServiceImpl;
import com.frontop.terminal.service.impl.SystemAudioServiceImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 消息工厂
 * </P>
 *
 * @author :CCC
 * @since :2021-05-26 16:58:00
 **/
public class MessageDisposeFactory {

    private static final Map<String, ICommandSerivce> MESSAGE_MAP;

    static {
        MESSAGE_MAP = new HashMap<>();
        // CMD类型命令
        MESSAGE_MAP.put(CommandTypeConst.CMD, new CmdCommandServiceImpl());
        //播放列表类型命令
        MESSAGE_MAP.put(CommandTypeConst.PLAYER_PLAY_LIST, new PlayListServiceImpl());
        //播放类型命令
        MESSAGE_MAP.put(CommandTypeConst.PLAYER_PLAY, new PlayServiceImpl());
        //音量类型命令
        MESSAGE_MAP.put(CommandTypeConst.PLAYER_AUDIO, new AudioServiceImpl());
        //系统音量类型命令
        MESSAGE_MAP.put(CommandTypeConst.SYSTEM_AUDIO, new SystemAudioServiceImpl());
        //程序检测更新类型命令
        MESSAGE_MAP.put(CommandTypeConst.CHECK_UPDATE, new ProcedureUpdateServiceImpl());
        //播放器状态类型命令
        MESSAGE_MAP.put(CommandTypeConst.PLAYER_STATUS, new PlayerStatusServiceImpl());
        //移除播放列表资源
        MESSAGE_MAP.put(CommandTypeConst.PLAYER_REMOVE_RESOURCE, new PlayerRemoveResourceServiceImpl());
        //移除互动程序资源
        MESSAGE_MAP.put(CommandTypeConst.PROGRAM_REMOVE_RESOURCE, new ProgramRemoveServiceImpl());
        //修改互动程序名称
        MESSAGE_MAP.put(CommandTypeConst.PROGRAM_UPDATE_NAME, new ProgramUpdateNameServiceImpl());
        //发布播放列表资源
        MESSAGE_MAP.put(CommandTypeConst.PLAYER_PUBLISH_RESOURCE, new PlayerPublishResourceServiceImpl());
        //发布互动程序资源
        MESSAGE_MAP.put(CommandTypeConst.PROGRAM_PUBLISH_RESOURCE, new ProcedurePublishResourceServiceImpl());
        //修改资源名称
        MESSAGE_MAP.put(CommandTypeConst.PLAYER_UPDATE_RESOURCE_NAME, new PlayerUpdateResourceNameServiceImpl());
        //系统授权许可
        MESSAGE_MAP.put(CommandTypeConst.SYSTEM_LICENSE, new LicenseServiceImpl());
    }

    /**
     * 获取业务处理对象
     *
     * @param key 命令
     * @return ICommandService
     */
    public static ICommandSerivce getWorkers(String key) {
        return MESSAGE_MAP.get(key);
    }
}
