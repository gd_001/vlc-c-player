package com.frontop.terminal.util;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.frontop.terminal.entity.AwaitInform;
import com.frontop.terminal.entity.File;
import com.frontop.terminal.entity.PlayList;
import com.frontop.terminal.entity.Program;
import com.frontop.terminal.entity.TerminalConfig;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 写入文件工具
 * </P>
 *
 * @author :CCC
 * @since :2021-06-09 11:42:00
 **/
@SuppressWarnings("unchecked")
public class WriteDataUtil {

    public static void write(String text, String path) {
        save(path, text);
    }

    public static void write(Object obj, Integer coding) {
        if (Objects.isNull(obj) || Objects.isNull(coding)) {
            System.err.println("WriteDataUtil.write 方法参数为空");
            return;
        }

        String path;
        String text;
        switch (coding) {
            case 0:
                ReadDataUtil.PROGRAM_LIST = (List<Program>) obj;
                path = ReadDataUtil.TERMINAL_PROGRAM_LIST_PATH;
                text = JSON.toJSONString(ReadDataUtil.PROGRAM_LIST);
                break;
            case 1:
                ReadDataUtil.FILE_LIST = (List<File>) obj;
                path = ReadDataUtil.TERMINAL_FILE_PATH;
                text = JSON.toJSONString(ReadDataUtil.FILE_LIST);
                break;
            case 2:
                ReadDataUtil.PLAYLIST_LIST = (List<PlayList>) obj;
                path = ReadDataUtil.TERMINAL_PLAY_LIST_PATH;
                text = JSON.toJSONString(ReadDataUtil.PLAYLIST_LIST);
                break;
            case 3:
                ReadDataUtil.AWAITINFORM_LIST = (List<AwaitInform>) obj;
                path = ReadDataUtil.TERMINAL_AWAIT_INFORM_PATH;
                text = JSON.toJSONString(ReadDataUtil.AWAITINFORM_LIST);
                break;
            case 4:
                ReadDataUtil.TERMINAL_CONFIG = (TerminalConfig) obj;
                path = ReadDataUtil.TERMINAL_CONFIG_PATH;
                text = JSON.toJSONString(ReadDataUtil.TERMINAL_CONFIG);
                break;
            default:
                System.err.println("未知coding，保存失败");
                return;
        }


        save(path, text);
    }

    private static void save(String path, String text) {
        if (StrUtil.isEmpty(text) || StrUtil.isEmpty(path)) {
            throw new RuntimeException("parameter text is null OR path null");
        }

        try (OutputStream outputStream = new FileOutputStream(path);
             OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
             BufferedWriter writer = new BufferedWriter(outputStreamWriter)) {
            writer.write(text);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
