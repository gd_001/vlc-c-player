package com.frontop.terminal.rmonclient.app;


import com.frontop.terminal.rmonclient.client.Client;
import com.frontop.terminal.rmonclient.compress.RLEncoding;
import com.frontop.terminal.rmonclient.hid.KeyMapping;
import com.frontop.terminal.rmonclient.system.LocalComputer;
import org.springframework.context.annotation.DependsOn;

/**
 * @author matrixy 优化：ccc
 * @date 2018/4/9 2021/6/4
 */

//@Component
@DependsOn("readDataUtil")
public class Tentacle {
    public Tentacle() {
        initCore();
    }

    /**
     * 核心模块初始化，提高运行中的性能
     */
    private void initCore() {

        KeyMapping.init();

        // 静态成员初始化
        RLEncoding.init();

        LocalComputer.init();

        new Client().start();
    }
}
