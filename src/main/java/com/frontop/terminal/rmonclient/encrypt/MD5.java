package com.frontop.terminal.rmonclient.encrypt;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Objects;

/**
 * @author matrixy 优化：ccc
 * @date 2018/4/9 2021/6/4
 */
public class MD5 {
    public static void main(String[] args) {
        System.out.println(Objects.requireNonNull(MD5.encode("web:::127.0.0.1:::29:::;S,I(\\\"**%k#/:!7D")).length());
    }

    public static String encode(String s) {
        try {
            return md5(s.getBytes(StandardCharsets.UTF_8));
        } catch (Exception ex) {
            return null;
        }
    }

    @SuppressWarnings("unused")
    public static String encode(byte[] buf) {
        return md5(buf);
    }

    private static String md5(byte[] btInput) {
        char[] hexDigits = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            mdInst.update(btInput);
            byte[] md = mdInst.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (byte byte0 : md) {
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
