package com.frontop.terminal.rmonclient.hid;

/**
 * 鼠标命令
 *
 * @author ccc
 */
public class MouseCommand extends HIDCommand {
    public static final int MOUSE_DOWN = 0x01;
    public static final int MOUSE_UP = 0x02;
    public static final int MOUSE_MOVE = 0x03;
    public static final int MOUSE_WHEEL = 0x04;

    /**
     * 事件类型，按下，放开，移动
     */
    public int eventType;

    /**
     * 1左键，2中键，3右键，或1 向上，2向下
     */
    public int key;
    public int x;
    public int y;

    public MouseCommand(int eventType, int key, int x, int y, int timestamp) {
        super(TYPE_MOUSE, timestamp);
        this.eventType = eventType & 0xff;
        this.key = key & 0xff;
        this.x = x;
        this.y = y;
    }
}
