package com.frontop.terminal.rmonclient.hid;

import java.awt.event.KeyEvent;

/**
 * @author matrixy 2018/4/22.
 * 键映射
 */
public class KeyMapping {
    static final int[] MAPPINGS = new int[256];

    public static void init() {
        MAPPINGS[13] = KeyEvent.VK_ENTER;
        MAPPINGS[8] = KeyEvent.VK_BACK_SPACE;
        MAPPINGS[9] = KeyEvent.VK_TAB;
        MAPPINGS[16] = KeyEvent.VK_SHIFT;
        MAPPINGS[17] = KeyEvent.VK_CONTROL;
        MAPPINGS[18] = KeyEvent.VK_ALT;
        MAPPINGS[20] = KeyEvent.VK_CAPS_LOCK;
        MAPPINGS[27] = KeyEvent.VK_ESCAPE;
        MAPPINGS[32] = KeyEvent.VK_SPACE;
        MAPPINGS[33] = KeyEvent.VK_PAGE_UP;
        MAPPINGS[34] = KeyEvent.VK_PAGE_DOWN;
        MAPPINGS[35] = KeyEvent.VK_END;
        MAPPINGS[36] = KeyEvent.VK_HOME;
        MAPPINGS[37] = KeyEvent.VK_LEFT;
        MAPPINGS[38] = KeyEvent.VK_UP;
        MAPPINGS[39] = KeyEvent.VK_RIGHT;
        MAPPINGS[40] = KeyEvent.VK_DOWN;
        MAPPINGS[188] = KeyEvent.VK_COMMA;
        MAPPINGS[189] = KeyEvent.VK_MINUS;
        MAPPINGS[190] = KeyEvent.VK_PERIOD;
        MAPPINGS[191] = KeyEvent.VK_SLASH;
        MAPPINGS[186] = KeyEvent.VK_SEMICOLON;
        MAPPINGS[187] = KeyEvent.VK_EQUALS;
        MAPPINGS[219] = KeyEvent.VK_OPEN_BRACKET;
        MAPPINGS[220] = KeyEvent.VK_BACK_SLASH;
        MAPPINGS[221] = KeyEvent.VK_CLOSE_BRACKET;
        MAPPINGS[96] = KeyEvent.VK_NUMPAD0;
        MAPPINGS[97] = KeyEvent.VK_NUMPAD1;
        MAPPINGS[98] = KeyEvent.VK_NUMPAD2;
        MAPPINGS[99] = KeyEvent.VK_NUMPAD3;
        MAPPINGS[100] = KeyEvent.VK_NUMPAD4;
        MAPPINGS[101] = KeyEvent.VK_NUMPAD5;
        MAPPINGS[102] = KeyEvent.VK_NUMPAD6;
        MAPPINGS[103] = KeyEvent.VK_NUMPAD7;
        MAPPINGS[104] = KeyEvent.VK_NUMPAD8;
        MAPPINGS[105] = KeyEvent.VK_NUMPAD9;
        MAPPINGS[106] = KeyEvent.VK_MULTIPLY;
        MAPPINGS[107] = KeyEvent.VK_ADD;
        MAPPINGS[46] = KeyEvent.VK_DELETE;
        MAPPINGS[144] = KeyEvent.VK_NUM_LOCK;
        MAPPINGS[112] = KeyEvent.VK_F1;
        MAPPINGS[113] = KeyEvent.VK_F2;
        MAPPINGS[114] = KeyEvent.VK_F3;
        MAPPINGS[115] = KeyEvent.VK_F4;
        MAPPINGS[116] = KeyEvent.VK_F5;
        MAPPINGS[117] = KeyEvent.VK_F6;
        MAPPINGS[118] = KeyEvent.VK_F7;
        MAPPINGS[119] = KeyEvent.VK_F8;
        MAPPINGS[120] = KeyEvent.VK_F9;
        MAPPINGS[121] = KeyEvent.VK_F10;
        MAPPINGS[122] = KeyEvent.VK_F11;
        MAPPINGS[123] = KeyEvent.VK_F12;
    }

    public static int convert(int code) {
        int key = code;
        if (MAPPINGS[code] > 0) {
            key = MAPPINGS[code];
        }
        return key;
    }
}


























