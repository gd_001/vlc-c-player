package com.frontop.terminal.rmonclient.hid;

/**
 * @author matrixy 优化：ccc
 * @date 2018/4/16 2021/6/4
 * 键盘命令
 */
public class KeyboardCommand extends HIDCommand {
    public static final int KEY_PRESS = 0x01;
    public static final int KEY_RELEASE = 0x02;

    public int keycode;
    public int eventType;

    public KeyboardCommand(int keycode, int eventType, int timestamp) {
        super(TYPE_KEYBOARD, timestamp);
        this.keycode = keycode;
        this.eventType = eventType;
    }
}
