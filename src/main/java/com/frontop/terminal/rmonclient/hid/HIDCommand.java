package com.frontop.terminal.rmonclient.hid;

/**
 * @author matrixy 优化：ccc
 * @date 2018/4/16 2021/6/4
 * 命令
 */
public class HIDCommand {
    public static final int TYPE_MOUSE = 1;
    public static final int TYPE_KEYBOARD = 2;

    public int type;
    public int timestamp;

    public HIDCommand(int type, int timestamp) {
        this.type = type;
        this.timestamp = timestamp;
    }
}
