package com.frontop.terminal.rmonclient.system.os;


import com.frontop.terminal.rmonclient.system.SystemCall;

/**
 * @author matrixy
 * @date 2018/5/8
 * windows自动登录
 */
public class Windows implements SystemCall {
    @Override
    public boolean login(String username, String password) throws Exception {
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec("rundll32.exe advapi32.dll,LogonUser  " + username + " " + password);
        process.waitFor();
        return process.exitValue() == 0;
    }

    @Override
    public boolean lock() throws Exception {
        Runtime.getRuntime().exec("RunDll32.exe user32.dll,LockWorkStation");
        return true;
    }
}
