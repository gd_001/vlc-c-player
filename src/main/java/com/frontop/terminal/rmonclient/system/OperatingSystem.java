package com.frontop.terminal.rmonclient.system;

import java.util.Date;

/**
 * @author matrixy 优化：ccc
 * @date 2018/5/8 2021/6/4
 */
public final class OperatingSystem {
    static OperatingSystem os;
    SystemCall systemCall;

    private OperatingSystem(SystemCall systemCall) {
        this.systemCall = systemCall;
    }

    public static synchronized OperatingSystem getInstance() throws Exception {
        if (null == os) {
            String osname = System.getProperty("os.name").replaceAll("^(\\w+)( .+)?$", "$1");
            os = new OperatingSystem((SystemCall) Class.forName("cn.org.hentai.tentacle.system.os." + osname).getDeclaredConstructor().newInstance());
        }
        return os;
    }

    public static void main(String[] args) throws Exception {
        OperatingSystem os = OperatingSystem.getInstance();
        os.lock();
        boolean rst;
        Thread.sleep(1000);
        rst = os.login("guodong", "asdfghjkl;'\\ ");
        System.out.println(rst);
        System.out.println(new Date());
    }

    public boolean login(String username, String password) throws Exception {
        return systemCall.login(username, password);
    }

    public void lock() throws Exception {
        systemCall.lock();
    }
}
