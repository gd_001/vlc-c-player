package com.frontop.terminal.rmonclient.system;


import com.frontop.terminal.rmonclient.graphic.Screenshot;

import java.awt.*;

/**
 * @author matrixy
 * @date 2018/4/9
 */
public final class LocalComputer {
    static Robot robot = null;

    /**
     * 创建整屏截图
     */
    public static Screenshot captureScreen() {
        return new Screenshot(robot.createScreenCapture(getScreenSize()));
    }

    /**
     * 获取屏幕分辨率
     */
    public static Rectangle getScreenSize() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        return new Rectangle((int) screenSize.getWidth(), (int) screenSize.getHeight());
    }

    public static void init() {
        try {
            robot = new Robot();
        } catch (AWTException ex) {
            throw new RuntimeException(ex);
        }
    }
}
