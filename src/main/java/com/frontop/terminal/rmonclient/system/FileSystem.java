package com.frontop.terminal.rmonclient.system;

import java.io.File;

/**
 * @author matrixy 优化：ccc
 * @date 2018/5/12 2021/6/4
 */
public final class FileSystem {
    /**
     * 列出本地文件
     *
     * @param path 文件目录，空字符串将列出根目录
     * @return 文件列表
     */
    public static File[] list(String path) {
        try {
            return "".equals(path) ? File.listRoots() : new File(path).listFiles();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new File[0];
    }
}
