package com.frontop.terminal.rmonclient.system;

/**
 * @author matrixy
 * @date 2018/5/8
 * 操作系统调用声明
 */
public interface SystemCall {

    /**
     * 用户登陆/解锁
     *
     * @param username 用户名
     * @param password 密码
     * @return 标注
     * @throws Exception 异常
     */
    boolean login(String username, String password) throws Exception;

    /**
     * 锁屏/锁定
     *
     * @return 标注
     * @throws Exception 异常
     */
    boolean lock() throws Exception;
}
