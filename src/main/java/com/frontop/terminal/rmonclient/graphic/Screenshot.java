package com.frontop.terminal.rmonclient.graphic;

import java.awt.image.BufferedImage;

/**
 * @author matrixy 优化：ccc
 * @date 2018/4/9 2021/6/4
 * 屏幕截图
 */
public final class Screenshot {
    /**
     * 截屏时间
     */
    public long captureTime;

    /**
     * 宽度
     */
    public int width;

    /**
     * 高度
     */
    public int height;

    /**
     * RGB
     */
    public int[] bitmap;

    /**
     * 压缩后的图像数据
     */
    public byte[] compressedData;

    @SuppressWarnings("unused")
    public Screenshot(int width, int height, long captureTime, byte[] compressedData) {
        this.width = width;
        this.height = height;
        this.captureTime = captureTime;
        this.compressedData = compressedData;
    }

    public Screenshot(BufferedImage img) {
        this.captureTime = System.currentTimeMillis();
        this.width = img.getWidth();
        this.height = img.getHeight();
        this.bitmap = img.getRGB(0, 0, this.width, this.height, null, 0, this.width);
    }

    /**
     * 截屏是否己过期，超过1秒的不需要再发送了
     *
     * @return boolean
     */
    public boolean isExpired() {
        return System.currentTimeMillis() - this.captureTime > 1000;
    }
}
