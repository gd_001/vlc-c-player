package com.frontop.terminal.rmonclient.util;

/**
 * @author matrixy
 * @date 2019/1/2
 */
public class Nonce {
    private static final String CHARACTERS = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String generate(int length) {
        char[] nonce = new char[length];
        for (int i = 0; i < nonce.length; i++) {
            nonce[i] = CHARACTERS.charAt((int) (Math.random() * CHARACTERS.length()));
        }
        return new String(nonce);
    }
}
