package com.frontop.terminal.rmonclient.worker;


import com.frontop.terminal.rmonclient.system.LocalComputer;
import com.frontop.terminal.rmonclient.util.Log;

/**
 * @author matrixy 优化：ccc
 * @date 2018/4/9 2021/6/4
 */
public class CaptureWorker extends BaseWorker {
    private void captureAndStore() {
        ScreenImages.addScreenshot(LocalComputer.captureScreen());
    }

    @Override
    public void run() {
        while (!this.isTerminated()) {
            try {
                captureAndStore();
                // TODO: FPS控制
                sleep(50);
            } catch (Exception e) {
                Log.error(e);
            }
        }
    }
}
