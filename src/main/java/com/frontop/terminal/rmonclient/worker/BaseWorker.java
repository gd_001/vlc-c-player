package com.frontop.terminal.rmonclient.worker;


import lombok.extern.slf4j.Slf4j;

/**
 * @author Expect 优化：ccc
 * @date 2018/4/19 2021/6/4
 * 基础工作者
 */
@Slf4j
public class BaseWorker extends Thread {
    /**
     * 终止状态
     */
    private boolean isTerminated = false;

    /**
     * 结束
     */
    public void terminate() {
        this.isTerminated = true;
        log.info("Terminate: " + this.getName() + ": " + this.getClass().getName());
    }

    public boolean isTerminated() {
        return this.isTerminated;
    }

    protected void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception e) {
        }
    }
}
