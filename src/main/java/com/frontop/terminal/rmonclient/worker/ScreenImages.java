package com.frontop.terminal.rmonclient.worker;


import com.frontop.terminal.rmonclient.graphic.Screenshot;
import com.frontop.terminal.rmonclient.protocol.Packet;

import java.util.LinkedList;

/**
 * @author matrixy 优化：ccc
 * @date 2018/4/9 2021/6/4
 */
public final class ScreenImages {
    static final LinkedList<Screenshot> SCREENSHOT_IMAGES = new LinkedList<>();
    static final LinkedList<Packet> COMPRESSED_SCREENS = new LinkedList<>();

    /**
     * 清空缓存
     */
    public static void clear() {
        synchronized (SCREENSHOT_IMAGES) {
            SCREENSHOT_IMAGES.clear();
        }
        synchronized (COMPRESSED_SCREENS) {
            COMPRESSED_SCREENS.clear();
        }
    }

    /**
     * 原始截图相关
     *
     * @param screenshot 屏幕截图
     */
    public static void addScreenshot(Screenshot screenshot) {
        synchronized (SCREENSHOT_IMAGES) {
            SCREENSHOT_IMAGES.addLast(screenshot);
        }
    }

    public static Screenshot getScreenshot() {
        synchronized (SCREENSHOT_IMAGES) {
            if (SCREENSHOT_IMAGES.size() == 0) {
                return null;
            }
            return SCREENSHOT_IMAGES.removeFirst();
        }
    }

    public static boolean hasScreenshots() {
        return SCREENSHOT_IMAGES.size() > 0;
    }

    /**
     * 压缩后的图像数据相关
     *
     * @return boolean
     */
    public static boolean hasCompressedScreens() {
        return COMPRESSED_SCREENS.size() > 0;
    }

    public static void addCompressedScreen(Packet packet) {
        synchronized (COMPRESSED_SCREENS) {
            COMPRESSED_SCREENS.addLast(packet);
        }
    }

    public static Packet getCompressedScreen() {
        synchronized (COMPRESSED_SCREENS) {
            if (COMPRESSED_SCREENS.size() == 0) {
                return null;
            }
            return COMPRESSED_SCREENS.removeFirst();
        }
    }
}
