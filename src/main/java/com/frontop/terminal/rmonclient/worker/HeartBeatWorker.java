package com.frontop.terminal.rmonclient.worker;


import com.frontop.terminal.rmonclient.client.Client;
import com.frontop.terminal.rmonclient.protocol.Command;
import com.frontop.terminal.rmonclient.protocol.Packet;

import java.io.IOException;

/**
 * @author :CCC
 * @create :2021-03-16 10:35:00
 **/
public class HeartBeatWorker extends BaseWorker {

    private final Client client;

    public HeartBeatWorker(Client client) {
        this.client = client;
    }

    @Override
    public void run() {
        while (!client.getConn().isClosed()) {
            try {
                Thread.sleep(5000);

                Packet p = Packet.create(Command.HEARTBEAT, 5);
                p.addBytes("HELLO".getBytes());
                client.send(p);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    client.getConn().close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
    }
}
