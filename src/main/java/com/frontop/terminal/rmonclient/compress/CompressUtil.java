package com.frontop.terminal.rmonclient.compress;

/**
 * @author matrixy
 * @date 2018/4/9
 * 压缩工具
 */
public final class CompressUtil {
    /**
     * 使用指定的压缩方法进行数据压缩
     *
     * @param method    压缩方法，如rle或huffman
     * @param argbArray ARGB序列的颜色数组
     * @return byte[]
     */
    public static byte[] process(String method, int[] argbArray, int from, int to) {
        return new RLEncoding().compress(argbArray, from, to);
    }
}
