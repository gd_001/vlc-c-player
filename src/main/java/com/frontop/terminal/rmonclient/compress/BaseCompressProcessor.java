package com.frontop.terminal.rmonclient.compress;

/**
 * @author matrixy
 * @date 2018/4/9
 * 基础压缩处理器
 */
public abstract class BaseCompressProcessor {

    /**
     * 压缩
     *
     * @param bitmap 位图
     * @param from   从……开始
     * @param to     到
     * @return byte[]
     */
    public abstract byte[] compress(int[] bitmap, int from, int to);
}
