package com.frontop.terminal.vlcjplayer.event;

/**
 * 显示特效事件
 *
 * @author Guodong
 */
public final class ShowEffectsEvent {

    public static final ShowEffectsEvent INSTANCE = new ShowEffectsEvent();
}
