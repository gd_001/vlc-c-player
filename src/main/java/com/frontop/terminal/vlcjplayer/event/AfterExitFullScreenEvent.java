package com.frontop.terminal.vlcjplayer.event;

/**
 * 后退事件
 *
 * @author ccc
 */
public final class AfterExitFullScreenEvent {

    public static final AfterExitFullScreenEvent INSTANCE = new AfterExitFullScreenEvent();
}
