package com.frontop.terminal.vlcjplayer.event;

/**
 * 停止事件
 *
 * @author Guodong
 */
public final class StoppedEvent {

    public static final StoppedEvent INSTANCE = new StoppedEvent();
}
