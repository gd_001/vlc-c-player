package com.frontop.terminal.vlcjplayer.event;

/**
 * 显示调试事件
 *
 * @author Guodong
 */
public final class ShowDebugEvent {

    public static final ShowDebugEvent INSTANCE = new ShowDebugEvent();
}
