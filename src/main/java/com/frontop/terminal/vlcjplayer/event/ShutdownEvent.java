package com.frontop.terminal.vlcjplayer.event;

/**
 * @author Guodong
 */
public final class ShutdownEvent {

    public static final ShutdownEvent INSTANCE = new ShutdownEvent();
}
