package com.frontop.terminal.vlcjplayer.event;

import uk.co.caprica.vlcj.player.renderer.RendererItem;

/**
 * 渲染器删除事件
 *
 * @author Guodong
 */
public final class RendererDeletedEvent {

    private final RendererItem rendererItem;

    public RendererDeletedEvent(RendererItem rendererItem) {
        this.rendererItem = rendererItem;
    }

    public RendererItem rendererItem() {
        return rendererItem;
    }

}
