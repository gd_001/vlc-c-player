package com.frontop.terminal.vlcjplayer.event;

/**
 * 显示信息事件
 *
 * @author Guodong
 */
public final class ShowMessagesEvent {

    public static final ShowMessagesEvent INSTANCE = new ShowMessagesEvent();
}
