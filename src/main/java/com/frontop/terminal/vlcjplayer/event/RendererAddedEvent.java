package com.frontop.terminal.vlcjplayer.event;

import uk.co.caprica.vlcj.player.renderer.RendererItem;

/**
 * 渲染器事件
 *
 * @author Guodong
 */
public final class RendererAddedEvent {

    private final RendererItem rendererItem;

    public RendererAddedEvent(RendererItem rendererItem) {
        this.rendererItem = rendererItem;
    }

    public RendererItem rendererItem() {
        return rendererItem;
    }

}
