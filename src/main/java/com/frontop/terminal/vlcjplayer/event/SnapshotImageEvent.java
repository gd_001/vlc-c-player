package com.frontop.terminal.vlcjplayer.event;

import java.awt.image.BufferedImage;

/**
 * 图片快照事件
 *
 * @author Guodong
 */
public final class SnapshotImageEvent {

    private final BufferedImage image;

    public SnapshotImageEvent(BufferedImage image) {
        this.image = image;
    }

    public BufferedImage image() {
        return image;
    }
}
