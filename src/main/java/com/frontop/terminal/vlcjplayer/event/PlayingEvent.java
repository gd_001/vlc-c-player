package com.frontop.terminal.vlcjplayer.event;

/**
 * 播放事件
 *
 * @author ccc
 */
public final class PlayingEvent {

    public static final PlayingEvent INSTANCE = new PlayingEvent();
}
