package com.frontop.terminal.vlcjplayer.event;

/**
 * 暂停事件
 *
 * @author ccc
 */
public final class PausedEvent {

    public static final PausedEvent INSTANCE = new PausedEvent();
}
