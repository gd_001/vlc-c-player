package com.frontop.terminal.vlcjplayer.event;

/**
 * 后退事件
 *
 * @author Guodong
 */
public final class BeforeEnterFullScreenEvent {

    public static final BeforeEnterFullScreenEvent INSTANCE = new BeforeEnterFullScreenEvent();
}
