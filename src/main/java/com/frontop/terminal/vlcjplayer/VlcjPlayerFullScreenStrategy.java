package com.frontop.terminal.vlcjplayer;

import com.frontop.terminal.vlcjplayer.event.AfterExitFullScreenEvent;
import com.frontop.terminal.vlcjplayer.event.BeforeEnterFullScreenEvent;
import uk.co.caprica.vlcj.player.embedded.fullscreen.adaptive.AdaptiveFullScreenStrategy;

import java.awt.*;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class VlcjPlayerFullScreenStrategy extends AdaptiveFullScreenStrategy {

    VlcjPlayerFullScreenStrategy(Window window) {
        super(window);
    }

    @Override
    protected void onBeforeEnterFullScreen() {
        application().post(BeforeEnterFullScreenEvent.INSTANCE);
    }

    @Override
    protected void onAfterExitFullScreen() {
        application().post(AfterExitFullScreenEvent.INSTANCE);
    }

}
