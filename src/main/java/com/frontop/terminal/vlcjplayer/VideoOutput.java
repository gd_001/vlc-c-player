package com.frontop.terminal.vlcjplayer;

/**
 * @author Guodong
 */

public enum VideoOutput {
    /**
     * 嵌入式
     */
    EMBEDDED,
    /**
     * 回调函数
     */
    CALLBACK

}
