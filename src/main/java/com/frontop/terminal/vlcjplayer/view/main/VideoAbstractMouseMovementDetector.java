package com.frontop.terminal.vlcjplayer.view.main;

import com.frontop.terminal.vlcjplayer.view.AbstractMouseMovementDetector;
import uk.co.caprica.vlcj.player.component.EmbeddedMediaPlayerComponent;

import java.awt.*;

final class VideoAbstractMouseMovementDetector extends AbstractMouseMovementDetector {

    private final EmbeddedMediaPlayerComponent mediaPlayerComponent;

    VideoAbstractMouseMovementDetector(Component component, int timeout, EmbeddedMediaPlayerComponent mediaPlayerComponent) {
        super(component, timeout);
        this.mediaPlayerComponent = mediaPlayerComponent;
    }

    @Override
    protected void onMouseAtRest() {
        mediaPlayerComponent.setCursorEnabled(false);
    }

    @Override
    protected void onMouseMoved() {
        mediaPlayerComponent.setCursorEnabled(true);
    }

    @Override
    protected void onStopped() {
        mediaPlayerComponent.setCursorEnabled(true);
    }
}
