package com.frontop.terminal.vlcjplayer.view.main;

import com.frontop.terminal.vlcjplayer.view.action.mediaplayer.SubtitleTrackActionAbstractMediaPlayerAction;
import uk.co.caprica.vlcj.player.base.TrackDescription;

import javax.swing.*;
import java.util.List;

import static com.frontop.terminal.vlcjplayer.Application.application;
import static com.frontop.terminal.vlcjplayer.view.action.Resource.resource;

final class SubtitleAbstractTrackMenu extends AbstractTrackMenu {

    SubtitleAbstractTrackMenu() {
        super(resource("menu.subtitle.item.track"));
    }

    @Override
    protected Action createAction(TrackDescription trackDescription) {
        return new SubtitleTrackActionAbstractMediaPlayerAction(trackDescription.description(), trackDescription.id());
    }

    @Override
    protected List<TrackDescription> onGetTrackDescriptions() {
        return application().mediaPlayer().subpictures().trackDescriptions();
    }

    @Override
    protected int onGetSelectedTrack() {
        return application().mediaPlayer().subpictures().track();
    }
}
