package com.frontop.terminal.vlcjplayer.view.main;

import com.frontop.terminal.vlcjplayer.view.image.ImagePane;
import uk.co.caprica.vlcj.player.embedded.VideoSurfaceApi;

import javax.swing.*;
import java.awt.*;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class VideoContentPane extends JPanel {

    private static final String NAME_DEFAULT = "default";

    private static final String NAME_VIDEO = "video";

    private static final String NAME_CALLBACK_VIDEO = "callback-video";

    private final CardLayout cardLayout;

    VideoContentPane() {
        cardLayout = new CardLayout();
        setLayout(cardLayout);

        VideoSurfaceApi playerG = application().callbackMediaPlayerComponent().mediaPlayer().videoSurface();
        playerG.attachVideoSurface();

        //设置播放器背景透明度0
        add(new ImagePane(ImagePane.Mode.CENTER, getClass().getResource("/static/img/vlcj-logo.png"), 0), NAME_DEFAULT);

        add(application().mediaPlayerComponent(), NAME_VIDEO);
        add(application().callbackMediaPlayerComponent(), NAME_CALLBACK_VIDEO);
    }

    public void showDefault() {
        cardLayout.show(this, NAME_DEFAULT);
    }

    public void showVideo() {
        switch (application().videoOutput()) {
            case EMBEDDED:
                cardLayout.show(this, NAME_VIDEO);
                break;
            case CALLBACK:
                cardLayout.show(VideoContentPane.this, NAME_CALLBACK_VIDEO);
                break;
            default:
                throw new IllegalStateException();
        }
    }

}
