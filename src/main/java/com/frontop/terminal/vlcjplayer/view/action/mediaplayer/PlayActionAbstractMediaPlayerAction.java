package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class PlayActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    PlayActionAbstractMediaPlayerAction(Resource resource) {
        super(resource);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!application().mediaPlayer().status().isPlaying()) {
            application().mediaPlayer().controls().play();
        } else {
            application().mediaPlayer().controls().pause();
        }
    }

}
