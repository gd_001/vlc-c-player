package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.AbstractStandardAction;
import com.frontop.terminal.vlcjplayer.view.action.Resource;

/**
 * 媒体播放器动作
 */
abstract class AbstractMediaPlayerAction extends AbstractStandardAction {

    private static final long serialVersionUID = -7664346666970230140L;

    AbstractMediaPlayerAction(Resource resource) {
        super(resource);
    }

    AbstractMediaPlayerAction(String name) {
        super(name);
    }

}
