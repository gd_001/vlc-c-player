package com.frontop.terminal.vlcjplayer.view.main;

import com.frontop.terminal.vlcjplayer.view.action.mediaplayer.TitleActionAbstractMediaPlayerAction;
import uk.co.caprica.vlcj.player.base.TitleDescription;
import uk.co.caprica.vlcj.player.base.TrackDescription;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

import static com.frontop.terminal.vlcjplayer.Application.application;
import static com.frontop.terminal.vlcjplayer.view.action.Resource.resource;

final class TitleAbstractTrackMenu extends AbstractTrackMenu {

    TitleAbstractTrackMenu() {
        super(resource("menu.playback.item.title"));
    }

    @Override
    protected Action createAction(TrackDescription trackDescription) {
        return new TitleActionAbstractMediaPlayerAction(trackDescription.description(), trackDescription.id());
    }

    @Override
    protected List<TrackDescription> onGetTrackDescriptions() {
        // FIXME for now I'll just convert the list... but it should be List<TitleDescription>
        List<TitleDescription> titles = application().mediaPlayer().titles().titleDescriptions();
        List<TrackDescription> result = new ArrayList<TrackDescription>(titles.size());
        int id = 0;
        for (TitleDescription title : titles) {
            if (!title.isMenu()) {
                String name = title.name();
                if (name == null) {
                    name = String.format("Feature %d", id + 1);
                }
                // FIXME use Duration from TitleDescription
                result.add(new TrackDescription(id++, name));
            }
        }
        return result;
    }

    @Override
    protected int onGetSelectedTrack() {
        return application().mediaPlayer().titles().title();
    }
}
