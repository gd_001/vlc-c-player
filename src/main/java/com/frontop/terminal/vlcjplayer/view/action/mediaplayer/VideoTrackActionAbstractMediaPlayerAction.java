package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * @author ccc
 */
public final class VideoTrackActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private static final long serialVersionUID = 3349791880721188270L;
    private final int trackId;

    public VideoTrackActionAbstractMediaPlayerAction(String name, int trackId) {
        super(name);
        this.trackId = trackId;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().video().setTrack(trackId);
    }

}
