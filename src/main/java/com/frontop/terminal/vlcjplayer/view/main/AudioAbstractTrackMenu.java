package com.frontop.terminal.vlcjplayer.view.main;

import com.frontop.terminal.vlcjplayer.view.action.mediaplayer.AudioTrackActionAbstractMediaPlayerAction;
import uk.co.caprica.vlcj.player.base.TrackDescription;

import javax.swing.*;
import java.util.List;

import static com.frontop.terminal.vlcjplayer.Application.application;
import static com.frontop.terminal.vlcjplayer.view.action.Resource.resource;

final class AudioAbstractTrackMenu extends AbstractTrackMenu {

    AudioAbstractTrackMenu() {
        super(resource("menu.audio.item.track"));
    }

    @Override
    protected Action createAction(TrackDescription trackDescription) {
        return new AudioTrackActionAbstractMediaPlayerAction(trackDescription.description(), trackDescription.id());
    }

    @Override
    protected List<TrackDescription> onGetTrackDescriptions() {
        return application().mediaPlayer().audio().trackDescriptions();
    }

    @Override
    protected int onGetSelectedTrack() {
        return application().mediaPlayer().audio().track();
    }
}
