package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.event.SnapshotImageEvent;
import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class SnapshotActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    SnapshotActionAbstractMediaPlayerAction(Resource resource) {
        super(resource);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        BufferedImage image = application().mediaPlayer().snapshots().get();
        if (image != null) {
            application().post(new SnapshotImageEvent(image));
        }
    }

}
