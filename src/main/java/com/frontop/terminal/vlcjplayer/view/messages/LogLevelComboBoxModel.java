package com.frontop.terminal.vlcjplayer.view.messages;

import uk.co.caprica.vlcj.log.LogLevel;

import javax.swing.*;

@SuppressWarnings("serial")
final class LogLevelComboBoxModel extends DefaultComboBoxModel<LogLevel> {

    LogLevelComboBoxModel() {
        for (LogLevel value : LogLevel.values()) {
            addElement(value);
        }
    }
}
