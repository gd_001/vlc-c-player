package com.frontop.terminal.vlcjplayer.view.main;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import com.frontop.terminal.component.ResourcePublishing;
import com.frontop.terminal.constant.PlayStatusConst;
import com.frontop.terminal.entity.TerminalConfig;
import com.frontop.terminal.util.FileUtils;
import com.frontop.terminal.util.ReadDataUtil;
import com.frontop.terminal.vlcjplayer.VlcjPlayer;
import com.frontop.terminal.vlcjplayer.event.AfterExitFullScreenEvent;
import com.frontop.terminal.vlcjplayer.event.BeforeEnterFullScreenEvent;
import com.frontop.terminal.vlcjplayer.event.PausedEvent;
import com.frontop.terminal.vlcjplayer.event.PlayingEvent;
import com.frontop.terminal.vlcjplayer.event.RendererAddedEvent;
import com.frontop.terminal.vlcjplayer.event.RendererDeletedEvent;
import com.frontop.terminal.vlcjplayer.event.ShowDebugEvent;
import com.frontop.terminal.vlcjplayer.event.ShowEffectsEvent;
import com.frontop.terminal.vlcjplayer.event.ShowMessagesEvent;
import com.frontop.terminal.vlcjplayer.event.SnapshotImageEvent;
import com.frontop.terminal.vlcjplayer.event.StoppedEvent;
import com.frontop.terminal.vlcjplayer.view.BaseFrame;
import com.frontop.terminal.vlcjplayer.view.action.AbstractStandardAction;
import com.frontop.terminal.vlcjplayer.view.action.mediaplayer.MediaPlayerActions;
import com.frontop.terminal.vlcjplayer.view.action.mediaplayer.RendererActionAbstract;
import com.frontop.terminal.vlcjplayer.view.snapshot.SnapshotView;
import com.google.common.eventbus.Subscribe;
import net.miginfocom.swing.MigLayout;
import uk.co.caprica.vlcj.media.MediaSlaveType;
import uk.co.caprica.vlcj.media.TrackType;
import uk.co.caprica.vlcj.player.base.Logo;
import uk.co.caprica.vlcj.player.base.LogoPosition;
import uk.co.caprica.vlcj.player.base.Marquee;
import uk.co.caprica.vlcj.player.base.MarqueePosition;
import uk.co.caprica.vlcj.player.base.MediaPlayer;
import uk.co.caprica.vlcj.player.base.MediaPlayerEventAdapter;
import uk.co.caprica.vlcj.player.base.MediaPlayerEventListener;
import uk.co.caprica.vlcj.player.renderer.RendererItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.prefs.Preferences;

import static com.frontop.terminal.vlcjplayer.Application.application;
import static com.frontop.terminal.vlcjplayer.Application.resources;
import static com.frontop.terminal.vlcjplayer.view.action.Resource.resource;

/**
 * @author Guodong
 */
@SuppressWarnings({"FieldCanBeLocal", "MoveFieldAssignmentToInitializer", "ConstantConditions", "UnstableApiUsage", "unused"})
public final class MainFrame extends BaseFrame {

    private static final String ACTION_EXIT_FULLSCREEN = "exit-fullscreen";

    private static final KeyStroke KEYSTROKE_ESCAPE = KeyStroke.getKeyStroke("ESCAPE");

    private static final KeyStroke KEYSTROKE_TOGGLE_FULLSCREEN = KeyStroke.getKeyStroke("F11");

    private final Action mediaOpenAction;
    private final Action mediaQuitAction;

    private final Action playbackRendererLocalAction;

    private final AbstractStandardAction videoFullscreenAction;
    private final AbstractStandardAction videoAlwaysOnTopAction;

    private final Action subtitleAddSubtitleFileAction;

    private final Action toolsEffectsAction;
    private final Action toolsMessagesAction;
    private final Action toolsDebugAction;

    private final AbstractStandardAction viewStatusBarAction;

    private final Action helpAboutAction;

    private final JMenuBar menuBar;

    private final JMenu mediaMenu;
    private final JMenu mediaRecentMenu;

    private final JMenu playbackMenu;
    private final JMenu playbackTitleMenu;
    private final JMenu playbackChapterMenu;
    private final JMenu playbackRendererMenu;
    private final JMenu playbackSpeedMenu;

    private final JMenu audioMenu;
    private final JMenu audioTrackMenu;
    private final JMenu audioDeviceMenu;
    private final JMenu audioStereoMenu;

    private final JMenu videoMenu;
    private final JMenu videoTrackMenu;
    private final JMenu videoZoomMenu;
    private final JMenu videoAspectRatioMenu;
    private final JMenu videoCropMenu;
    private final JMenu videoOutputMenu;

    private final JMenu subtitleMenu;
    private final JMenu subtitleTrackMenu;

    private final JMenu toolsMenu;

    private final JMenu viewMenu;

    private final JMenu helpMenu;

    private final JFileChooser fileChooser;

    private final PositionPane positionPane;

    private final ControlsPane controlsPane;

    private final StatusBar statusBar;

    private final VideoContentPane videoContentPane;

    private final JPanel bottomPane;

//    private final AbstractMouseMovementDetector mouseMovementDetector;

    private final List<RendererItem> renderers = new ArrayList<>();

    public MainFrame() {
        super("vlcj player");

        MediaPlayerActions mediaPlayerActions = application().mediaPlayerActions();

        mediaOpenAction = new AbstractStandardAction(resource("menu.media.item.openFile")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("选择文件");
                if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(MainFrame.this)) {
                    File file = fileChooser.getSelectedFile();
                    String mrl = file.getAbsolutePath();
                    VlcjPlayer.play(mrl);
                }
            }
        };

        mediaQuitAction = new AbstractStandardAction(resource("menu.media.item.quit")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                System.exit(0);
            }
        };

        playbackRendererLocalAction = new AbstractStandardAction(resource("menu.playback.item.renderer.item.local")) {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                application().mediaPlayer().setRenderer(null);
            }
        };

        videoFullscreenAction = new AbstractStandardAction(resource("menu.video.item.fullscreen")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                application().mediaPlayer().fullScreen().toggle();
            }
        };

        videoAlwaysOnTopAction = new AbstractStandardAction(resource("menu.video.item.alwaysOnTop")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean onTop;
                Object source = e.getSource();
                if (source instanceof JCheckBoxMenuItem) {
                    JCheckBoxMenuItem menuItem = (JCheckBoxMenuItem) source;
                    onTop = menuItem.isSelected();
                } else {
                    throw new IllegalStateException("Don't know about source " + source);
                }
                setAlwaysOnTop(onTop);
            }
        };

        subtitleAddSubtitleFileAction = new AbstractStandardAction(resource("menu.subtitle.item.addSubtitleFile")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(MainFrame.this)) {
                    System.out.println("最近播放");
                    File file = fileChooser.getSelectedFile();
                    application().mediaPlayer().media().addSlave(MediaSlaveType.SUBTITLE, String.format("file://%s", file.getAbsolutePath()), true);
                }
            }
        };

        toolsEffectsAction = new AbstractStandardAction(resource("menu.tools.item.effects")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                application().post(ShowEffectsEvent.INSTANCE);
            }
        };

        toolsMessagesAction = new AbstractStandardAction(resource("menu.tools.item.messages")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                application().post(ShowMessagesEvent.INSTANCE);
            }
        };

        toolsDebugAction = new AbstractStandardAction(resource("menu.tools.item.debug")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                application().post(ShowDebugEvent.INSTANCE);
            }
        };

        viewStatusBarAction = new AbstractStandardAction(resource("menu.view.item.statusBar")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean visible;
                Object source = e.getSource();
                if (source instanceof JCheckBoxMenuItem) {
                    JCheckBoxMenuItem menuItem = (JCheckBoxMenuItem) source;
                    visible = menuItem.isSelected();
                } else {
                    throw new IllegalStateException("Don't know about source " + source);
                }
                statusBar.setVisible(visible);
                bottomPane.invalidate();
                bottomPane.revalidate();
                bottomPane.getParent().invalidate();
                bottomPane.getParent().revalidate();
                MainFrame.this.invalidate();
                MainFrame.this.revalidate();
            }
        };

        helpAboutAction = new AbstractStandardAction(resource("menu.help.item.about")) {
            @Override
            public void actionPerformed(ActionEvent e) {
                AboutDialog dialog = new AboutDialog(MainFrame.this);
                dialog.setLocationRelativeTo(MainFrame.this);

                dialog.setVisible(true);
            }
        };

        menuBar = new JMenuBar();

        mediaMenu = new JMenu(resource("menu.media").name());
        mediaMenu.setMnemonic(resource("menu.media").mnemonic());
        mediaMenu.add(new JMenuItem(mediaOpenAction));
        mediaRecentMenu = new RecentMediaMenuAbstract(resource("menu.media.item.recent")).menu();
        mediaMenu.add(mediaRecentMenu);
        mediaMenu.add(new JSeparator());
        mediaMenu.add(new JMenuItem(mediaQuitAction));
        menuBar.add(mediaMenu);

        playbackMenu = new JMenu(resource("menu.playback").name());
        playbackMenu.setMnemonic(resource("menu.playback").mnemonic());

        playbackTitleMenu = new TitleAbstractTrackMenu().menu();

        // Chapter could be an "on-demand" menu too, and it could be with radio buttons...

        playbackMenu.add(playbackTitleMenu);

        playbackChapterMenu = new ChapterMenuAbstract().menu();
        playbackMenu.add(playbackChapterMenu);
        playbackMenu.add(new JSeparator());

        playbackRendererMenu = new JMenu(resource("menu.playback.item.renderer").name());
        playbackRendererMenu.setMnemonic(resource("menu.playback.item.renderer").mnemonic());
        playbackRendererMenu.add(new JMenuItem(playbackRendererLocalAction));
        playbackMenu.add(playbackRendererMenu);

        playbackMenu.add(new JSeparator());

        playbackSpeedMenu = new JMenu(resource("menu.playback.item.speed").name());
        playbackSpeedMenu.setMnemonic(resource("menu.playback.item.speed").mnemonic());
        for (Action action : mediaPlayerActions.playbackSpeedActions()) {
            playbackSpeedMenu.add(new JMenuItem(action));
        }
        playbackMenu.add(playbackSpeedMenu);
        playbackMenu.add(new JSeparator());
        for (Action action : mediaPlayerActions.playbackSkipActions()) {
            playbackMenu.add(new JMenuItem(action));
        }
        playbackMenu.add(new JSeparator());
        for (Action action : mediaPlayerActions.playbackChapterActions()) {
            playbackMenu.add(new JMenuItem(action));
        }
        playbackMenu.add(new JSeparator());
        for (Action action : mediaPlayerActions.playbackControlActions()) {
            // FIXME need a standardmenuitem that disables the tooltip like this, very poor show...
            playbackMenu.add(new JMenuItem(action) {
                @Override
                public String getToolTipText() {
                    return null;
                }
            });
        }
        menuBar.add(playbackMenu);

        audioMenu = new JMenu(resource("menu.audio").name());
        audioMenu.setMnemonic(resource("menu.audio").mnemonic());

        audioTrackMenu = new AudioAbstractTrackMenu().menu();

        audioMenu.add(audioTrackMenu);
        audioDeviceMenu = new AudioDeviceMenuAbstract().menu();
        audioMenu.add(audioDeviceMenu);
        audioStereoMenu = new JMenu(resource("menu.audio.item.stereoMode").name());
        audioStereoMenu.setMnemonic(resource("menu.audio.item.stereoMode").mnemonic());
        for (Action action : mediaPlayerActions.audioStereoModeActions()) {
            // FIXME the radio buttons are not clearing when the selection changes
            audioStereoMenu.add(new JRadioButtonMenuItem(action));
        }
        audioMenu.add(audioStereoMenu);
        audioMenu.add(new JSeparator());
        for (Action action : mediaPlayerActions.audioControlActions()) {
            audioMenu.add(new JMenuItem(action));
        }
        menuBar.add(audioMenu);

        videoMenu = new JMenu(resource("menu.video").name());
        videoMenu.setMnemonic(resource("menu.video").mnemonic());

        videoTrackMenu = new VideoAbstractTrackMenu().menu();

        videoMenu.add(videoTrackMenu);
        videoMenu.add(new JSeparator());
        videoMenu.add(new JCheckBoxMenuItem(videoFullscreenAction));
        videoMenu.add(new JCheckBoxMenuItem(videoAlwaysOnTopAction));
        videoMenu.add(new JSeparator());
        videoZoomMenu = new JMenu(resource("menu.video.item.zoom").name());
        videoZoomMenu.setMnemonic(resource("menu.video.item.zoom").mnemonic());
        // FIXME how to handle zoom 1:1 and fit to window - also, probably should not use addActions to select
        addActions(mediaPlayerActions.videoZoomActions(), videoZoomMenu);
        videoMenu.add(videoZoomMenu);
        videoAspectRatioMenu = new JMenu(resource("menu.video.item.aspectRatio").name());
        videoAspectRatioMenu.setMnemonic(resource("menu.video.item.aspectRatio").mnemonic());
        addActions(mediaPlayerActions.videoAspectRatioActions(), videoAspectRatioMenu, true);
        videoMenu.add(videoAspectRatioMenu);
        videoCropMenu = new JMenu(resource("menu.video.item.crop").name());
        videoCropMenu.setMnemonic(resource("menu.video.item.crop").mnemonic());
        addActions(mediaPlayerActions.videoCropActions(), videoCropMenu, true);
        videoMenu.add(videoCropMenu);
        videoMenu.add(new JSeparator());
        videoMenu.add(new JMenuItem(mediaPlayerActions.videoSnapshotAction()));
        videoMenu.add(new JSeparator());
        videoOutputMenu = new JMenu(resource("menu.video.item.output").name());
        videoOutputMenu.setMnemonic(resource("menu.video.item.output").mnemonic());
//        videoMenu-add(videoOutputMenu);
        menuBar.add(videoMenu);

        subtitleMenu = new JMenu(resource("menu.subtitle").name());
        subtitleMenu.setMnemonic(resource("menu.subtitle").mnemonic());
        subtitleMenu.add(new JMenuItem(subtitleAddSubtitleFileAction));

        subtitleTrackMenu = new SubtitleAbstractTrackMenu().menu();

        subtitleMenu.add(subtitleTrackMenu);
        menuBar.add(subtitleMenu);

        toolsMenu = new JMenu(resource("menu.tools").name());
        toolsMenu.setMnemonic(resource("menu.tools").mnemonic());
        toolsMenu.add(new JMenuItem(toolsEffectsAction));
        toolsMenu.add(new JMenuItem(toolsMessagesAction));
        toolsMenu.add(new JSeparator());
        toolsMenu.add(new JMenuItem(toolsDebugAction));
        menuBar.add(toolsMenu);

        viewMenu = new JMenu(resource("menu.view").name());
        viewMenu.setMnemonic(resource("menu.view").mnemonic());
        viewMenu.add(new JCheckBoxMenuItem(viewStatusBarAction));
        menuBar.add(viewMenu);

        helpMenu = new JMenu(resource("menu.help").name());
        helpMenu.setMnemonic(resource("menu.help").mnemonic());
        helpMenu.add(new JMenuItem(helpAboutAction));
        menuBar.add(helpMenu);

        TerminalConfig terminalConfig = ReadDataUtil.getTerminalConfig();
        if (Objects.nonNull(terminalConfig)) {
            //播放器工具栏不隐藏就显示,否则什么都不做
            if (!terminalConfig.isPlayerToolbarHide()) {
                setJMenuBar(menuBar);
            }
        } else {
            setJMenuBar(menuBar);
        }

        videoContentPane = new VideoContentPane();

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(videoContentPane);
        contentPane.setTransferHandler(new AbstractMediaTransferHandler() {
            @Override
            protected void onMediaDropped(String[] uris) {
                VlcjPlayer.play(uris[0]);
            }
        });

        setContentPane(contentPane);

        fileChooser = new JFileChooser();

        bottomPane = new JPanel();
        bottomPane.setLayout(new BorderLayout());

        JPanel bottomControlsPane = new JPanel();
        bottomControlsPane.setLayout(new MigLayout("fill, insets 0 n n n", "[grow]", "[]0[]"));

        positionPane = new PositionPane();
        bottomControlsPane.add(positionPane, "grow, wrap");

        controlsPane = new ControlsPane(mediaPlayerActions);
        bottomPane.add(bottomControlsPane, BorderLayout.CENTER);
        bottomControlsPane.add(controlsPane, "grow");

        statusBar = new StatusBar();
        bottomPane.add(statusBar, BorderLayout.SOUTH);

        if (Objects.nonNull(terminalConfig)) {
            //播放器工具栏不隐藏就显示,否则什么都不做
            if (!terminalConfig.isPlayerToolbarHide()) {
                contentPane.add(bottomPane, BorderLayout.SOUTH);
            }
        } else {
            contentPane.add(bottomPane, BorderLayout.SOUTH);
        }


        MediaPlayerEventListener mediaPlayerEventListener = (new MediaPlayerEventAdapter() {

            @Override
            public void elementaryStreamAdded(MediaPlayer mediaPlayer, TrackType type, int id) {
//                System.out.printf("ELEMENTARY STREAM ADDED %s %d%n", type, id);
            }

            @Override
            public void elementaryStreamSelected(MediaPlayer mediaPlayer, TrackType type, int id) {
//                System.out.printf("ELEMENTARY STREAM SELECTED %s %d%n", type, id);
            }

            @Override
            public void playing(MediaPlayer mediaPlayer) {
                SwingUtilities.invokeLater(() -> {
                    videoContentPane.showVideo();
//                        mouseMovementDetector.start();
                    application().post(PlayingEvent.INSTANCE);
                });
            }

            @Override
            public void paused(MediaPlayer mediaPlayer) {
                System.out.println("暂停");
                SwingUtilities.invokeLater(() -> {
//                        mouseMovementDetector.stop();
                    application().post(PausedEvent.INSTANCE);
                    long leng = application().mediaPlayer().status().length();
                    long tiem = application().mediaPlayer().status().time();
                    float f = application().mediaPlayer().status().position();
                    System.out.println("视频长度:" + leng + "当前:" + tiem + "百分之:" + f);
                });
            }

            @Override
            public void stopped(MediaPlayer mediaPlayer) {
                SwingUtilities.invokeLater(() -> {
//                        mouseMovementDetector.stop();
                    videoContentPane.showDefault();
                    application().post(StoppedEvent.INSTANCE);
                    System.out.println("停止播放");
                });
            }

            @Override
            public void finished(MediaPlayer mediaPlayer) {
                SwingUtilities.invokeLater(() -> {
                    videoContentPane.showDefault();

                    //获取播放模式 -列表循环，还是单个循环
                    TerminalConfig terminalConfig = ReadDataUtil.getTerminalConfig();
                    //设置默认播放模式
                    Integer palyPattern = PlayStatusConst.PLAYP_PATTERN_ON;
                    if (Objects.nonNull(terminalConfig) && Objects.nonNull(terminalConfig.getPlayMode())) {
                        //配置不为空的话根据配置文件设置
                        palyPattern = terminalConfig.getPlayMode();
                    }

                    if (palyPattern.equals(PlayStatusConst.PLAYP_PATTERN_LIST)) {
                        //列表循环播放-视频播放结束接着播放下一条视频
                        String resourcePath = ResourcePublishing.nextResource();
                        if (!StrUtil.isEmpty(resourcePath)) {
                            //播放视频
                            VlcjPlayer.play(resourcePath);
                        }
                    } else if (palyPattern.equals(PlayStatusConst.PLAYP_CURRENT_TO_END)) {
                        //仅仅播放当前视频直至当前视频结束

                        //启用待机画面的话
                        if (terminalConfig.isEnableStandby()) {
                            //获取待机资源
                            String standbyResource = ResourcePublishing.getStandbyResource();
                            //播放
                            application().addRecentMedia(standbyResource);
                            application().mediaPlayer().media().play(standbyResource);

                            boolean resourceType = FileUtils.isImg(FileUtil.getSuffix(standbyResource));
                            if (resourceType) {
                                ThreadUtil.sleep(600);
                                VlcjPlayer.pause();
                                System.out.println("当前播放是图片类型，自动暂停");
                            }
                        }
                    }

                    /*
                      单个循环模式已在StarTask中设置
                     */
                });
            }

            @Override
            public void error(MediaPlayer mediaPlayer) {
                SwingUtilities.invokeLater(() -> {
                    videoContentPane.showDefault();
                    application().post(StoppedEvent.INSTANCE);
                    // FIXME this is flawed with recent file menu
                    File selectedFile = fileChooser.getSelectedFile();
                    JOptionPane.showMessageDialog(MainFrame.this, MessageFormat.format(resources().getString("error.errorEncountered"), selectedFile != null ? selectedFile.getAbsolutePath() : ""), resources().getString("dialog.errorEncountered"), JOptionPane.ERROR_MESSAGE);
                });
            }

            @Override
            public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {
                SwingUtilities.invokeLater(() -> {
                    positionPane.setDuration(newLength);
                    statusBar.setDuration(newLength);
                });
            }

            @Override
            public void timeChanged(MediaPlayer mediaPlayer, long newTime) {
                SwingUtilities.invokeLater(() -> {
                    positionPane.setTime(newTime);
                    statusBar.setTime(newTime);
                });
            }

            @Override
            public void mediaPlayerReady(MediaPlayer mediaPlayer) {
            }
        });

        application().mediaPlayerComponent().mediaPlayer().events().addMediaPlayerEventListener(mediaPlayerEventListener);
        application().callbackMediaPlayerComponent().mediaPlayer().events().addMediaPlayerEventListener(mediaPlayerEventListener);

        getActionMap().put(ACTION_EXIT_FULLSCREEN, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                application().mediaPlayer().fullScreen().toggle();
                videoFullscreenAction.select(false);
            }
        });

        applyPreferences();

        setMinimumSize(new Dimension(370, 240));

    }

    @SuppressWarnings("SameParameterValue")
    private void addActions(List<Action> actions, JMenu menu, boolean selectFirst) {
        ButtonGroup buttonGroup = addActions(actions, menu);
        if (selectFirst) {
            Enumeration<AbstractButton> en = buttonGroup.getElements();
            if (en.hasMoreElements()) {
                AbstractStandardAction action = (AbstractStandardAction) en.nextElement().getAction();
                action.select(true);
            }
        }
    }

    private ButtonGroup addActions(List<Action> actions, JMenu menu) {
        ButtonGroup buttonGroup = new ButtonGroup();
        for (Action action : actions) {
            JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem(action);
            buttonGroup.add(menuItem);
            menu.add(menuItem);
        }
        return buttonGroup;
    }

    private void applyPreferences() {
        Preferences prefs = Preferences.userNodeForPackage(MainFrame.class);
        setBounds(
                prefs.getInt("frameX", 100),
                prefs.getInt("frameY", 100),
                prefs.getInt("frameWidth", 800),
                prefs.getInt("frameHeight", 600)
        );
        boolean alwaysOnTop = prefs.getBoolean("alwaysOnTop", false);
        setAlwaysOnTop(alwaysOnTop);
        videoAlwaysOnTopAction.select(alwaysOnTop);
        boolean statusBarVisible = prefs.getBoolean("statusBar", false);
        statusBar.setVisible(statusBarVisible);
        viewStatusBarAction.select(statusBarVisible);
        fileChooser.setCurrentDirectory(new File(prefs.get("chooserDirectory", ".")));
        String recentMedia = prefs.get("recentMedia", "");
        if (recentMedia.length() > 0) {
            List<String> mrls = Arrays.asList(prefs.get("recentMedia", "").split("\\|"));
            Collections.reverse(mrls);
            for (String mrl : mrls) {
                application().addRecentMedia(mrl);
            }
        }
    }

    @Override
    protected void onShutdown() {
        if (wasShown()) {
            Preferences prefs = Preferences.userNodeForPackage(MainFrame.class);
            prefs.putInt("frameX", getX());
            prefs.putInt("frameY", getY());
            prefs.putInt("frameWidth", getWidth());
            prefs.putInt("frameHeight", getHeight());
            prefs.putBoolean("alwaysOnTop", isAlwaysOnTop());
            prefs.putBoolean("statusBar", statusBar.isVisible());
            prefs.put("chooserDirectory", fileChooser.getCurrentDirectory().toString());

            String recentMedia;
            List<String> mrls = application().recentMedia();
            if (!mrls.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (String mrl : mrls) {
                    if (sb.length() > 0) {
                        sb.append('|');
                    }
                    sb.append(mrl);
                }
                recentMedia = sb.toString();
            } else {
                recentMedia = "";
            }
            prefs.put("recentMedia", recentMedia);
        }
    }

    @Subscribe
    public void onBeforeEnterFullScreen(BeforeEnterFullScreenEvent event) {
        menuBar.setVisible(false);
        bottomPane.setVisible(false);
        // As the menu is now hidden, the shortcut will not work, so register a temporary key-binding
        registerEscapeBinding();
    }

    @Subscribe
    public void onAfterExitFullScreen(AfterExitFullScreenEvent event) {
        deregisterEscapeBinding();
        menuBar.setVisible(true);
        bottomPane.setVisible(true);
    }

    @Subscribe
    public void onSnapshotImage(SnapshotImageEvent event) {
        new SnapshotView(event.image());
    }

    @Subscribe
    public void onRendererAdded(RendererAddedEvent event) {
        synchronized (renderers) {
            renderers.add(event.rendererItem());
        }
        updateRenderersMenu();
    }

    @Subscribe
    public void onRendererDeleted(RendererDeletedEvent event) {
        synchronized (renderers) {
            for (RendererItem renderer : renderers) {
                if (renderer.rendererItemInstance().equals(event.rendererItem().rendererItemInstance())) {
                    renderers.remove(renderer);
                    break;
                }
            }
        }
        updateRenderersMenu();
    }

    private void updateRenderersMenu() {
        SwingUtilities.invokeLater(() -> {
            synchronized (renderers) {
                for (int i = 1; i < playbackRendererMenu.getItemCount(); i++) {
                    playbackRendererMenu.remove(i);
                }

                for (RendererItem renderer : renderers) {
                    playbackRendererMenu.add(new RendererActionAbstract(renderer.name(), renderer));
                }
            }
        });
    }

    private void registerEscapeBinding() {
        getInputMap().put(KEYSTROKE_ESCAPE, ACTION_EXIT_FULLSCREEN);
        getInputMap().put(KEYSTROKE_TOGGLE_FULLSCREEN, ACTION_EXIT_FULLSCREEN);
    }

    private void deregisterEscapeBinding() {
        getInputMap().remove(KEYSTROKE_ESCAPE);
        getInputMap().remove(KEYSTROKE_TOGGLE_FULLSCREEN);
    }

    private InputMap getInputMap() {
        JComponent c = (JComponent) getContentPane();
        return c.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    private ActionMap getActionMap() {
        JComponent c = (JComponent) getContentPane();
        return c.getActionMap();
    }

    private void setLogoAndMarquee(MediaPlayer mediaPlayer) {
        Logo logo = Logo.logo()
                .file("src/main/resources/static/img/vlcj-logo.png")
                .position(LogoPosition.TOP_LEFT)
                .opacity(0.5f)
                .enable();

        mediaPlayer.logo().set(logo);

        Marquee marquee = Marquee.marquee()
                .text("vlcj-player")
                .colour(Color.white.getRGB())
                .size(20)
                .position(MarqueePosition.BOTTOM_RIGHT)
                .location(10, 5)
                .enable();

        mediaPlayer.marquee().set(marquee);
    }
}
