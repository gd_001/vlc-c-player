package com.frontop.terminal.vlcjplayer.view.main;

import com.frontop.terminal.vlcjplayer.VlcjPlayer;
import com.frontop.terminal.vlcjplayer.view.AbstractOnDemandMenu;
import com.frontop.terminal.vlcjplayer.view.action.AbstractStandardAction;
import com.frontop.terminal.vlcjplayer.view.action.Resource;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.List;

import static com.frontop.terminal.vlcjplayer.Application.application;
import static com.frontop.terminal.vlcjplayer.view.action.Resource.resource;

final class RecentMediaMenuAbstract extends AbstractOnDemandMenu {

    RecentMediaMenuAbstract(Resource resource) {
        super(resource, true);
    }

    @Override
    protected final void onPrepareMenu(JMenu menu) {
        List<String> mrls = application().recentMedia();
        if (!mrls.isEmpty()) {
            int i = 1;
            for (String mrl : mrls) {
                menu.add(new PlayRecentAction(i++, mrl));
            }
            menu.add(new JSeparator());
        }
        menu.add(new ClearRecentMediaActionAbstract());
    }

    private class PlayRecentAction extends AbstractAction {

        private static final long serialVersionUID = 3608512334449574844L;
        private final String mrl;

        public PlayRecentAction(int number, String mrl) {
            super(String.format("%d: %s", number, mrl));
            putValue(Action.MNEMONIC_KEY, number < 10 ? number : 0);
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(String.format("control %d", number < 10 ? number : 0)));
            this.mrl = mrl;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            // FIXME something odd going on when using renderer (e.g. chromecast), if you play again while it's already casting you get an error callback
            //       and this thread actually seems to die if you invoke stop first, i.e. single step the stop call and it executes but never returns
            //       but if you press the stop button first, it seems ok...
            VlcjPlayer.play(mrl);
        }
    }

    private class ClearRecentMediaActionAbstract extends AbstractStandardAction {

        private static final long serialVersionUID = 3049378210571714475L;

        public ClearRecentMediaActionAbstract() {
            super(resource("menu.media.item.recent.item.clear"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            application().clearRecentMedia();
        }
    }
}
