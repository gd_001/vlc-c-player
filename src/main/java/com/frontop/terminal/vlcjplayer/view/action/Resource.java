package com.frontop.terminal.vlcjplayer.view.action;

import javax.swing.*;
import java.util.Objects;

import static com.frontop.terminal.vlcjplayer.Application.resources;

/**
 * @author Guodong
 */
public final class Resource {

    private final String id;

    private Resource(String id) {
        this.id = id;
    }

    public static Resource resource(String id) {
        return new Resource(id);
    }

    public String name() {
        if (resources().containsKey(id)) {
            return resources().getString(id);
        } else {
            return null;
        }
    }

    public int mnemonic() {
        String key = id + ".mnemonic";
        if (resources().containsKey(key)) {
            return resources().getString(key).charAt(0);
        } else {
            return 0;
        }
    }

    public KeyStroke shortcut() {
        String key = id + ".shortcut";
        if (resources().containsKey(key)) {
            return KeyStroke.getKeyStroke(resources().getString(key));
        } else {
            return null;
        }
    }

    public String tooltip() {
        String key = id + ".tooltip";
        if (resources().containsKey(key)) {
            return resources().getString(key);
        } else {
            return null;
        }
    }

    public Icon menuIcon() {
        String key = id + ".menuIcon";
        if (resources().containsKey(key)) {
            return new ImageIcon(Objects.requireNonNull(getClass().getResource("/icons/actions/" + resources().getString(key) + ".png")));
        } else {
            return null;
        }
    }

    public Icon buttonIcon() {
        String key = id + ".buttonIcon";
        if (resources().containsKey(key)) {
            return new ImageIcon(Objects.requireNonNull(getClass().getResource("/icons/buttons/" + resources().getString(key) + ".png")));
        } else {
            return null;
        }
    }
}
