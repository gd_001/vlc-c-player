package com.frontop.terminal.vlcjplayer.view.action;

import javax.swing.*;

/**
 * @author Guodong
 */
public abstract class AbstractStandardAction extends AbstractAction {

    private static final long serialVersionUID = -507645545961941033L;

    public AbstractStandardAction(Resource resource) {
        putValue(Action.NAME, resource.name());
        putValue(Action.MNEMONIC_KEY, resource.mnemonic());
        putValue(Action.ACCELERATOR_KEY, resource.shortcut());
        putValue(Action.SHORT_DESCRIPTION, resource.tooltip());
        putValue(Action.SMALL_ICON, resource.menuIcon());
        putValue(Action.LARGE_ICON_KEY, resource.buttonIcon());
    }

    public AbstractStandardAction(String name) {
        putValue(Action.NAME, name);
    }

    public final void select(boolean select) {
        putValue(Action.SELECTED_KEY, select);
    }
}
