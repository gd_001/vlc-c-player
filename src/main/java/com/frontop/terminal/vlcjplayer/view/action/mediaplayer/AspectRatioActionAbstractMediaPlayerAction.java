package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * 方面比率动作
 */
final class AspectRatioActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private final String aspectRatio;

    AspectRatioActionAbstractMediaPlayerAction(Resource resource, String aspectRatio) {
        super(resource);
        this.aspectRatio = aspectRatio;
    }

    AspectRatioActionAbstractMediaPlayerAction(String name, String aspectRatio) {
        super(name);
        this.aspectRatio = aspectRatio;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().video().setAspectRatio(aspectRatio);
    }

}
