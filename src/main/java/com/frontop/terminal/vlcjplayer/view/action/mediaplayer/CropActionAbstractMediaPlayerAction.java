package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class CropActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private final String cropGeometry;

    CropActionAbstractMediaPlayerAction(Resource resource, String cropGeometry) {
        super(resource);
        this.cropGeometry = cropGeometry;
    }

    public CropActionAbstractMediaPlayerAction(String name, String cropGeometry) {
        super(name);
        this.cropGeometry = cropGeometry;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().video().setCropGeometry(cropGeometry);
    }

}
