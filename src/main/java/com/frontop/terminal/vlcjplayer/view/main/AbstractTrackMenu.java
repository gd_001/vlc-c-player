package com.frontop.terminal.vlcjplayer.view.main;

import com.frontop.terminal.vlcjplayer.view.AbstractOnDemandMenu;
import com.frontop.terminal.vlcjplayer.view.action.Resource;
import uk.co.caprica.vlcj.player.base.TrackDescription;

import javax.swing.*;
import java.util.List;

/**
 * @author ccc
 */
abstract class AbstractTrackMenu extends AbstractOnDemandMenu {

    private static final String KEY_TRACK_DESCRIPTION = "track-description";

    AbstractTrackMenu(Resource resource) {
        super(resource, true);
    }

    @Override
    protected final void onPrepareMenu(JMenu menu) {
        ButtonGroup buttonGroup = new ButtonGroup();
        int selectedTrack = onGetSelectedTrack();
        for (TrackDescription trackDescription : onGetTrackDescriptions()) {
            JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem(createAction(trackDescription));
            menuItem.putClientProperty(KEY_TRACK_DESCRIPTION, trackDescription);
            buttonGroup.add(menuItem);
            menu.add(menuItem);
            if (selectedTrack == trackDescription.id()) {
                menuItem.setSelected(true);
            }
        }
    }

    /**
     * 创建Action
     *
     * @param trackDescription Action
     * @return 音轨描述
     */
    protected abstract Action createAction(TrackDescription trackDescription);

    /**
     * 获取多个音轨对象
     *
     * @return List<TrackDescription>
     */
    protected abstract List<TrackDescription> onGetTrackDescriptions();

    /**
     * 获取正在使用的音轨对象数
     *
     * @return int
     */
    protected abstract int onGetSelectedTrack();
}
