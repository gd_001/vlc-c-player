package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * @author Guodong
 */
public final class TitleActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private static final long serialVersionUID = -1698552383370426151L;
    private final int titleId;

    public TitleActionAbstractMediaPlayerAction(String name, int titleId) {
        super(name);
        this.titleId = titleId;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().titles().setTitle(titleId);
    }

}
