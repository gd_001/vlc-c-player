package com.frontop.terminal.vlcjplayer.view.main;

import com.frontop.terminal.vlcjplayer.view.AbstractOnDemandMenu;
import com.frontop.terminal.vlcjplayer.view.action.mediaplayer.ChapterActionAbstractMediaPlayerAction;
import uk.co.caprica.vlcj.player.base.ChapterDescription;
import uk.co.caprica.vlcj.player.base.MediaPlayer;

import javax.swing.*;
import java.util.List;

import static com.frontop.terminal.vlcjplayer.Application.application;
import static com.frontop.terminal.vlcjplayer.view.action.Resource.resource;

// FIXME there's no reason this couldn't be another radiobutton menu... and show the current chapter - probably more useful that way even if not the same as VLC

final class ChapterMenuAbstract extends AbstractOnDemandMenu {

    ChapterMenuAbstract() {
        super(resource("menu.playback.item.chapter"), true);
    }

    @Override
    protected void onPrepareMenu(JMenu menu) {
        MediaPlayer mediaPlayer = application().mediaPlayer();
        List<ChapterDescription> chapters = mediaPlayer.chapters().descriptions();
        if (chapters != null && !chapters.isEmpty()) {
            int i = 0;
            for (ChapterDescription chapter : chapters) {
                String name = chapter.name();
                if (name == null) {
                    name = String.format("Chapter %02d", i + 1);
                }
                long offset = chapter.offset() / 1000 / 60;
                long duration = chapter.duration() / 1000 / 60;
                String s = String.format("%s %dm (%dm)", name, offset, duration);
                JMenuItem menuItem = new JMenuItem(new ChapterActionAbstractMediaPlayerAction(s, i++));
                menu.add(menuItem);
            }
        }
    }
}
