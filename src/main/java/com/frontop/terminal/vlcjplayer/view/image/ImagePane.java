package com.frontop.terminal.vlcjplayer.view.image;

import cn.hutool.core.util.NumberUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

/**
 * 绘制背景图像的面板。
 *
 * @author Guodong
 */
public final class ImagePane extends JComponent {

    private static final long serialVersionUID = 1L;
    private final Mode mode;
    private final float opacity;
    private BufferedImage sourceImage;
    private BufferedImage image;
    private int lastWidth;
    private int lastHeight;

    public ImagePane(Mode mode, URL imageUrl, float opacity) {
        this.mode = mode;
        this.opacity = opacity;
        newImage(imageUrl);
        prepareImage();
    }

    public ImagePane(Mode mode, BufferedImage image, float opacity) {
        this.mode = mode;
        this.opacity = opacity;
        this.sourceImage = image;
        prepareImage();
    }

    @Override
    public Dimension getPreferredSize() {
        return sourceImage != null ? new Dimension(sourceImage.getWidth(), sourceImage.getHeight()) : super.getPreferredSize();
    }

    @Override
    protected void paintComponent(Graphics g) {
        prepareImage();
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.black);
        g2.fill(getBounds());
        if (image != null) {
            int x = 0;
            int y = 0;
            if (mode != Mode.DEFAULT) {
                x = (getWidth() - image.getWidth()) / 2;
                y = (getHeight() - image.getHeight()) / 2;
            }

            Composite oldComposite = g2.getComposite();
            float l = 1.0f;
            if (!NumberUtil.equals(opacity, l)) {
                g2.setComposite(AlphaComposite.SrcOver.derive(opacity));
            }
            g2.drawImage(image, null, x, y);
            g2.setComposite(oldComposite);
        }
        g2.dispose();
    }

    @Override
    public boolean isOpaque() {
        return true;
    }

    private void newImage(URL imageUrl) {
        image = null;
        if (imageUrl != null) {
            try {
                sourceImage = ImageIO.read(imageUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void prepareImage() {
        if (lastWidth != getWidth() || lastHeight != getHeight()) {
            lastWidth = getWidth();
            lastHeight = getHeight();
            if (sourceImage != null) {
                switch (mode) {
                    case DEFAULT:
                    case CENTER:
                        image = sourceImage;
                        break;
                    case FIT:
                        image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
                        Graphics2D g2 = image.createGraphics();
                        AffineTransform at = AffineTransform.getScaleInstance((double) image.getWidth() / sourceImage.getWidth(), (double) image.getHeight() / sourceImage.getHeight());
                        g2.drawRenderedImage(sourceImage, at);
                        g2.dispose();
                        break;
                    default:
                        break;
                }
            } else {
                image = null;
            }
        }
    }

    public enum Mode {
        /**
         * 默认
         */
        DEFAULT,
        /**
         * 居中
         */
        CENTER,
        /**
         * 适合
         */
        FIT
    }
}
