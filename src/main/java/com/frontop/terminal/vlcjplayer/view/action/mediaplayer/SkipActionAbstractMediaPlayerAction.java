package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class SkipActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private final long delta;

    SkipActionAbstractMediaPlayerAction(Resource resource, long delta) {
        super(resource);
        this.delta = delta;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().controls().skipTime(delta);
    }

}
