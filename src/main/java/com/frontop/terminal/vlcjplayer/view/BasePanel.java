package com.frontop.terminal.vlcjplayer.view;

import com.frontop.terminal.vlcjplayer.event.ShutdownEvent;
import com.google.common.eventbus.Subscribe;

import javax.swing.*;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * @author ccc
 */
public abstract class BasePanel extends JPanel {

    public BasePanel() {
        application().subscribe(this);
    }

    @Subscribe
    public final void onShutdown(ShutdownEvent event) {
        onShutdown();
    }

    /**
     * Override, e.g. to save component preferences.
     */
    protected final void onShutdown() {
    }
}
