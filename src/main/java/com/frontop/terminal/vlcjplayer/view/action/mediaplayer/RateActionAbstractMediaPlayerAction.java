package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class RateActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private final float rate;

    RateActionAbstractMediaPlayerAction(Resource resource, float rate) {
        super(resource);
        this.rate = rate;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().controls().setRate(rate);
    }

}
