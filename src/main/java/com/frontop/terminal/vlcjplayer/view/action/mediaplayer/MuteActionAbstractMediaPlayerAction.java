package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class MuteActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    MuteActionAbstractMediaPlayerAction(Resource resource) {
        super(resource);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().audio().mute();
    }

}
