package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class StopActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    StopActionAbstractMediaPlayerAction(Resource resource) {
        super(resource);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().controls().stop();
    }

}
