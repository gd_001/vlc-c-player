package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class ZoomActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private final float zoom;

    ZoomActionAbstractMediaPlayerAction(Resource resource, float zoom) {
        super(resource);
        this.zoom = zoom;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().video().setScale(zoom);
    }

}
