package com.frontop.terminal.vlcjplayer.view.effects.audio;

import com.frontop.terminal.vlcjplayer.view.BasePanel;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;

import static com.frontop.terminal.vlcjplayer.Application.resources;

/**
 * @author Guodong
 */
public class AudioEffectsPanel extends BasePanel {

    public AudioEffectsPanel() {
        JTabbedPane tabbedPane = new JTabbedPane();

        EqualizerPanel equalizerPanel = new EqualizerPanel();
        tabbedPane.add(equalizerPanel, resources().getString("dialog.effects.tabs.audio.equalizer"));

        setLayout(new MigLayout("fill", "grow", "grow"));
        add(tabbedPane, "grow");
    }
}
