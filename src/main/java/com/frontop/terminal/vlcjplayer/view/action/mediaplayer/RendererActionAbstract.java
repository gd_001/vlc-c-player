package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.AbstractStandardAction;
import uk.co.caprica.vlcj.player.renderer.RendererItem;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URL;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * @author Guodong
 */
public final class RendererActionAbstract extends AbstractStandardAction {

    private static final long serialVersionUID = 3337979976465600196L;
    private final RendererItem renderer;

    public RendererActionAbstract(String name, RendererItem renderer) {
        super(name);
        this.renderer = renderer;
        try {
            putValue(Action.SMALL_ICON, new ImageIcon(ImageIO.read(new URL(renderer.iconUri()))));
        } catch (IOException e) {
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().setRenderer(renderer);
    }

}
