package com.frontop.terminal.vlcjplayer.view.messages;

import uk.co.caprica.vlcj.log.LogLevel;

final class NativeLogMessage {

    private final String module;
    private final String name;
    private final LogLevel level;
    private final String message;

    NativeLogMessage(String module, String name, LogLevel level, String message) {
        this.module = module;
        this.name = name;
        this.level = level;
        this.message = message;
    }

    public String getModule() {
        return module;
    }

    public String getName() {
        return name;
    }

    public LogLevel getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }
}
