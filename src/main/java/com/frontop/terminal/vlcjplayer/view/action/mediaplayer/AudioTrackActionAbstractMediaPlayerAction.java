package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * 音频踪迹动作
 *
 * @author Guodong
 */
public final class AudioTrackActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private static final long serialVersionUID = 4984841313397392254L;
    private final int trackId;

    public AudioTrackActionAbstractMediaPlayerAction(String name, int trackId) {
        super(name);
        this.trackId = trackId;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().audio().setTrack(trackId);
    }

}
