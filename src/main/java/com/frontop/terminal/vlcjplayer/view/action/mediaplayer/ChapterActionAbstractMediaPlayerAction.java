package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * 播放记录动作
 *
 * @author Guodong
 */
public final class ChapterActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private static final long serialVersionUID = -8456707809285054909L;
    private final int chapter;

    public ChapterActionAbstractMediaPlayerAction(String name, int chapter) {
        super(name);
        this.chapter = chapter;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().chapters().setChapter(chapter);
    }

}
