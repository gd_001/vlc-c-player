package com.frontop.terminal.vlcjplayer.view.effects.video;

import com.frontop.terminal.vlcjplayer.view.BasePanel;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;

import static com.frontop.terminal.vlcjplayer.Application.resources;

/**
 * @author Guodong
 */
public class VideoEffectsPanel extends BasePanel {

    private final JTabbedPane tabbedPane;

    private final VideoAdjustPanel videoAdjustPanel;

    public VideoEffectsPanel() {
        tabbedPane = new JTabbedPane();

        videoAdjustPanel = new VideoAdjustPanel();
        tabbedPane.add(videoAdjustPanel, resources().getString("dialog.effects.tabs.video.adjust"));

        setLayout(new MigLayout("fill", "grow", "grow"));
        add(tabbedPane, "grow");
    }

}
