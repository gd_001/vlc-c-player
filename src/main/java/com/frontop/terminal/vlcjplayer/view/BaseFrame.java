package com.frontop.terminal.vlcjplayer.view;

import com.frontop.terminal.vlcjplayer.event.ShutdownEvent;
import com.google.common.eventbus.Subscribe;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.IOException;
import java.util.Objects;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * @author Guodong
 */
public abstract class BaseFrame extends JFrame {

    private boolean wasShown;

    public BaseFrame(String title) {
        super(title);
        try {
            setIconImage(ImageIO.read(Objects.requireNonNull(getClass().getResource("/static/img/vlcj-logo.png"))));
        } catch (IOException ignored) {
        }
        application().subscribe(this);
    }

    @Override
    public final void setVisible(boolean b) {
        super.setVisible(b);
        if (b) {
            this.wasShown = true;
        }
    }

    @Subscribe
    public final void onShutdown(ShutdownEvent event) {
        onShutdown();
    }

    protected final boolean wasShown() {
        return wasShown;
    }

    /**
     * Override, e.g. to save component preferences.
     */
    protected void onShutdown() {
    }
}
