package com.frontop.terminal.vlcjplayer.view.main;

import net.miginfocom.swing.MigLayout;
import uk.co.caprica.vlcj.support.Info;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;
import static com.frontop.terminal.vlcjplayer.Application.resources;

final class AboutDialog extends JDialog {

    AboutDialog(Window owner) {
        super(owner, resources().getString("dialog.about"), ModalityType.DOCUMENT_MODAL);

        setLayout(new MigLayout("insets 30, fillx", "[shrink]30[shrink][grow]", "[]30[]10[]10[]30[]10[]10[]10[]0[]"));
        getContentPane().setBackground(Color.white);

        JLabel logoLabel = new JLabel();
        logoLabel.setIcon(new ImageIcon(getClass().getResource("/static/img/vlcj-logo.png")));

        JLabel applicationLabel = new JLabel();
        applicationLabel.setFont(applicationLabel.getFont().deriveFont(30.0f));
        applicationLabel.setText(resources().getString("dialog.about.application"));

        JLabel blurb1Label = new JLabel();
        blurb1Label.setText(resources().getString("dialog.about.blurb1"));

        JLabel blurb2Label = new JLabel();
        blurb2Label.setText(resources().getString("dialog.about.blurb2"));

        JLabel attribution1Label = new JLabel();
        attribution1Label.setText(resources().getString("dialog.about.attribution1"));

        JLabel applicationVersionLabel = new JLabel();
        applicationVersionLabel.setText(resources().getString("dialog.about.applicationVersion"));

        JLabel applicationVersionValueLabel = new ValueLabel();
        applicationVersionValueLabel.setText("1.0");

        JLabel vlcjVersionLabel = new JLabel();
        vlcjVersionLabel.setText(resources().getString("dialog.about.vlcjVersion"));

        JLabel vlcjVersionValueLabel = new ValueLabel();
        vlcjVersionValueLabel.setText(Info.getInstance().vlcjVersion().toString());

        JLabel vlcVersionLabel = new JLabel();
        vlcVersionLabel.setText(resources().getString("dialog.about.vlcVersion"));

        JLabel nativeLibraryPathLabel = new JLabel();
        nativeLibraryPathLabel.setText(resources().getString("dialog.about.nativeLibraryPath"));

        JLabel nativeLibraryPathValueLabel = new ValueLabel();
        nativeLibraryPathValueLabel.setText(application().mediaPlayerComponent().mediaPlayerFactory().nativeLibraryPath());

        JLabel vlcVersionValueLabel = new ValueLabel();
        vlcVersionValueLabel.setText(application().mediaPlayerComponent().mediaPlayerFactory().application().version());

        JLabel vlcChangesetValueLabel = new ValueLabel();
        vlcChangesetValueLabel.setText(application().mediaPlayerComponent().mediaPlayerFactory().application().changeset());

        add(logoLabel, "shrink, top, spany 8");
        add(applicationLabel, "grow, spanx 2, wrap");
        add(blurb1Label, "grow, spanx 2, wrap");
        add(blurb2Label, "grow, spanx 2, wrap");
        add(attribution1Label, "grow, spanx 2, wrap");
        add(applicationVersionLabel, "");
        add(applicationVersionValueLabel, "wrap");
        add(vlcjVersionLabel);
        add(vlcjVersionValueLabel, "wrap");
        add(nativeLibraryPathLabel, "");
        add(nativeLibraryPathValueLabel, "wrap");
        add(vlcVersionLabel);
        add(vlcVersionValueLabel, "wrap");
        add(vlcChangesetValueLabel, "skip 2");

        getRootPane().registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);

        pack();
        setResizable(false);
    }

    private class ValueLabel extends JLabel {

        public ValueLabel() {
            setFont(getFont().deriveFont(Font.BOLD));
        }
    }
}
