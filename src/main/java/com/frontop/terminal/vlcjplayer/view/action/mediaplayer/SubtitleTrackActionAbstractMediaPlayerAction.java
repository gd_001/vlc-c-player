package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * @author Guodong
 */
public final class SubtitleTrackActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private static final long serialVersionUID = 6265418644736937055L;
    private final int trackId;

    public SubtitleTrackActionAbstractMediaPlayerAction(String name, int trackId) {
        super(name);
        this.trackId = trackId;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().subpictures().setTrack(trackId);
    }

}
