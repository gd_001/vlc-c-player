package com.frontop.terminal.vlcjplayer.view.main;

import cn.hutool.core.util.StrUtil;
import com.frontop.terminal.component.ResourcePublishing;
import com.frontop.terminal.vlcjplayer.VlcjPlayer;
import com.frontop.terminal.vlcjplayer.event.PausedEvent;
import com.frontop.terminal.vlcjplayer.event.PlayingEvent;
import com.frontop.terminal.vlcjplayer.event.ShowEffectsEvent;
import com.frontop.terminal.vlcjplayer.event.StoppedEvent;
import com.frontop.terminal.vlcjplayer.view.BasePanel;
import com.frontop.terminal.vlcjplayer.view.action.mediaplayer.MediaPlayerActions;
import com.google.common.eventbus.Subscribe;
import net.miginfocom.swing.MigLayout;
import uk.co.caprica.vlcj.player.base.LibVlcConst;

import javax.swing.*;
import java.util.Objects;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * @author ccc
 */
@SuppressWarnings({"FieldCanBeLocal", "UnstableApiUsage", "unused"})
public final class ControlsPane extends BasePanel {

    private static JSlider volumeSlider;
    private final Icon playIcon = newIcon("play");
    private final Icon pauseIcon = newIcon("pause");
    private final Icon previousIcon = newIcon("previous");
    private final Icon nextIcon = newIcon("next");
    private final Icon fullscreenIcon = newIcon("fullscreen");
    private final Icon extendedIcon = newIcon("extended");
    private final Icon snapshotIcon = newIcon("snapshot");
    private final Icon volumeHighIcon = newIcon("volume-high");
    private final Icon volumeMutedIcon = newIcon("volume-muted");
    private final JButton playPauseButton;
    private final JButton previousButton;
    private final JButton stopButton;
    private final JButton nextButton;
    private final JButton fullscreenButton;
    private final JButton extendedButton;
    private final JButton snapshotButton;
    private final JButton muteButton;
    private final int sum = 0;

    ControlsPane(MediaPlayerActions mediaPlayerActions) {
        playPauseButton = new BigButton();
        playPauseButton.setAction(mediaPlayerActions.playbackPlayAction());
        previousButton = new StandardButton();
        previousButton.setIcon(previousIcon);
        stopButton = new StandardButton();
        stopButton.setAction(mediaPlayerActions.playbackStopAction());
        nextButton = new StandardButton();
        nextButton.setIcon(nextIcon);
        fullscreenButton = new StandardButton();
        fullscreenButton.setIcon(fullscreenIcon);
        extendedButton = new StandardButton();
        extendedButton.setIcon(extendedIcon);
        snapshotButton = new StandardButton();
        snapshotButton.setAction(mediaPlayerActions.videoSnapshotAction());
        muteButton = new StandardButton();
        muteButton.setIcon(volumeHighIcon);
        volumeSlider = new JSlider();
        volumeSlider.setMinimum(LibVlcConst.MIN_VOLUME);
        volumeSlider.setMaximum(LibVlcConst.MAX_VOLUME);

        setLayout(new MigLayout("fill, insets 0 0 0 0", "[]12[]0[]0[]12[]0[]12[]push[]0[]", "[]"));

        add(playPauseButton);
        add(previousButton, "sg 1");
        add(stopButton, "sg 1");
        add(nextButton, "sg 1");

        add(fullscreenButton, "sg 1");
        add(extendedButton, "sg 1");

        add(snapshotButton, "sg 1");

        add(muteButton, "sg 1");
        add(volumeSlider, "wmax 100");

        volumeSlider.addChangeListener(e -> application().mediaPlayer().audio().setVolume(volumeSlider.getValue()));

        // FIXME really these should share common actions

        muteButton.addActionListener(e -> application().mediaPlayer().audio().mute());

        fullscreenButton.addActionListener(e -> application().mediaPlayer().fullScreen().toggle());

        extendedButton.addActionListener(e -> application().post(ShowEffectsEvent.INSTANCE));

        nextButton.addActionListener(e -> {
            System.out.println("下一个视频");
            //下一个
            String next = ResourcePublishing.nextResource();
            if (!StrUtil.isEmpty(next)) {
                VlcjPlayer.play(next);
            }
        });

        previousButton.addActionListener(e -> {
            System.out.println("上一个视频");
            //下一个
            String next = ResourcePublishing.previousResource();
            if (!StrUtil.isEmpty(next)) {
                VlcjPlayer.play(next);
            }
        });
    }

    /**
     * 设置播放器音量条值
     *
     * @param value 值
     */
    public static void setVolumeSlider(Integer value) {
        volumeSlider.setValue(value);
    }

    @Subscribe
    public void onPlaying(PlayingEvent event) {
        playPauseButton.setIcon(pauseIcon);
    }

    @Subscribe
    public void onPaused(PausedEvent event) {
        playPauseButton.setIcon(playIcon);
    }

    @Subscribe
    public void onStopped(StoppedEvent event) {
        playPauseButton.setIcon(playIcon);
    }

    private Icon newIcon(String name) {
        return new ImageIcon(Objects.requireNonNull(getClass().getResource("/icons/buttons/" + name + ".png")));
    }

    private class BigButton extends JButton {

        private BigButton() {
            setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            setHideActionText(true);
        }
    }

    private class StandardButton extends JButton {

        private StandardButton() {
            setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
            setHideActionText(true);
        }
    }
}
