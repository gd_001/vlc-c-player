package com.frontop.terminal.vlcjplayer.view;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

/**
 * @author Guodong
 */
public class BorderedStandardLabel extends StandardLabel {

    private static final Border STANDARD_BORDER = BorderFactory.createCompoundBorder(
            BorderFactory.createBevelBorder(BevelBorder.LOWERED),
            BorderFactory.createEmptyBorder(1, 2, 1, 2)
    );

    public BorderedStandardLabel() {
        setBorder(STANDARD_BORDER);
    }

    public BorderedStandardLabel(String template) {
        super(template);
    }
}
