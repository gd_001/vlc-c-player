package com.frontop.terminal.vlcjplayer.view.debug;

import ca.odell.glazedlists.gui.TableFormat;

final class DebugMessageTableFormat implements TableFormat<DebugMessage> {

    private static final String[] COLUMN_NAMES = {
            "Message"
    };

    private static final int COLUMN_MESSAGE = 0;

    DebugMessageTableFormat() {
    }

    @Override
    public int getColumnCount() {
        return COLUMN_NAMES.length;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public Object getColumnValue(DebugMessage message, int column) {
        Object result;
        if (column == COLUMN_MESSAGE) {
            result = message.getMessage();
        } else {
            result = null;
        }
        return result;
    }
}
