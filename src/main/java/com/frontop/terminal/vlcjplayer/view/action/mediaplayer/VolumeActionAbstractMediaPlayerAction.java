package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class VolumeActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private final int delta;

    VolumeActionAbstractMediaPlayerAction(Resource resource, int delta) {
        super(resource);
        this.delta = delta;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().audio().setVolume(application().mediaPlayer().audio().volume() + delta);
    }

}
