package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import uk.co.caprica.vlcj.player.base.AudioDevice;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * 音频设备动作
 *
 * @author Guodong
 */
public final class AudioDeviceActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private static final long serialVersionUID = 6530595141977055066L;
    private final AudioDevice audioDevice;

    public AudioDeviceActionAbstractMediaPlayerAction(AudioDevice audioDevice) {
        super(audioDevice.getLongName());
        this.audioDevice = audioDevice;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().audio().setOutputDevice(null, audioDevice.getDeviceId());
    }

}
