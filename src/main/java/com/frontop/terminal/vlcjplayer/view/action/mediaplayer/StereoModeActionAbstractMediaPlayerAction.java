package com.frontop.terminal.vlcjplayer.view.action.mediaplayer;

import com.frontop.terminal.vlcjplayer.view.action.Resource;
import uk.co.caprica.vlcj.player.base.AudioChannel;

import java.awt.event.ActionEvent;

import static com.frontop.terminal.vlcjplayer.Application.application;

final class StereoModeActionAbstractMediaPlayerAction extends AbstractMediaPlayerAction {

    private final AudioChannel mode;

    StereoModeActionAbstractMediaPlayerAction(Resource resource, AudioChannel mode) {
        super(resource);
        this.mode = mode;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        application().mediaPlayer().audio().setChannel(mode);
    }

}
