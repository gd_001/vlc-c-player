package com.frontop.terminal.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 终端实体
 * </p>
 *
 * @author :CCC
 * @since :2021-05-17 17:31:00
 **/
@Data
@ToString
public class Terminal implements Serializable {

    private static final long serialVersionUID = 300968263098832376L;

    /**
     * 机器码
     */
    private String machineCode;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 操作系统版本
     */
    private String osVersions;

    /**
     * 操作系统位数
     */
    private Integer osBits;

    /**
     * 计算机名称
     */
    private String computerName;

    /**
     * 本机别名，用于服务器端更快定位
     */
    @JSONField(name = "terminalName")
    private String alias;

    /**
     * 已运行时间(秒)
     */
    private Long runTime;


    /**
     * cpu空闲率
     */
    private Double cpuLeisureRate;

    /**
     * cpu使用率
     */
    private Double cpuUseRate;

    /**
     * cpu序列号
     */
    private String processorId;

    /**
     * CPU型号信息
     */
    private String cpuInfo;

    /**
     * 可用物理内存
     */
    private Long usableMemory;

    /**
     * 总物理内存
     */
    private Long sumMemory;

    /**
     * ip
     */
    @JSONField(name = "terminalIp")
    private String ip;

    /**
     * 网卡mac
     */
    @JSONField(name = "terminalMac")
    private String mac;

    /**
     * 端口
     */
    @JSONField(name = "terminalPort")
    private Integer port;

    /**
     * 分辨率
     */
    private String screenSize;

    /**
     * 硬盘
     */
    private List<HardDisk> hardDiskList;

    /**
     * 传感器
     */
    private Sensors sensors;

    /**
     * 终端版本号
     */
    private String terminalVersion;

}
