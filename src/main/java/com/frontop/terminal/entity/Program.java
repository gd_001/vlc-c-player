package com.frontop.terminal.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 互动程序
 * </p>
 *
 * @author :CCC
 * @since :2021-07-22 11:36:00
 **/
@Data
public class Program implements Serializable {

    private static final long serialVersionUID = -4655732077571745378L;

    private String id;

    private String gangedId;

    /**
     * 资源路径
     */
    private String location;

    /**
     * 程序的名字(用户可自取，会显示在IPAD端上)
     */
    private String name;

    private Long fileId;

    /**
     * 来源 1中控 2手动导入
     */
    private Integer source;

    public Program(String id, String location, String name) {
        this.id = id;
        this.location = location;
        this.name = name;
    }

    public Program() {
    }
}
