package com.frontop.terminal.entity;

import lombok.Getter;

/**
 * <p>
 * 等待通知实体
 * </P>
 *
 * @author :CCC
 * @since :2021-07-02 10:46:00
 **/
@Getter
public class AwaitInform {

    private final Long mediaId;

    private final String mac;

    public AwaitInform(Long mediaId, String mac) {
        this.mediaId = mediaId;
        this.mac = mac;
    }
}
