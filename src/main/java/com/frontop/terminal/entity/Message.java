package com.frontop.terminal.entity;

import com.frontop.terminal.constant.CommandTypeConst;
import lombok.Data;

/**
 * <p>
 * 服务器与终端之间通讯实体
 * </P>
 *
 * @author :CCC
 * @since :2012-01-01 00:44:00
 * type类型查看{@link CommandTypeConst}
 **/
@Data
public class Message {

    /**
     * 信息类型
     */
    private String type;

    /**
     * 命令
     */
    private String command;

    public Message setType(String type) {
        this.type = type;
        return this;
    }

    public Message setCommand(String command) {
        this.command = command;
        return this;
    }
}
