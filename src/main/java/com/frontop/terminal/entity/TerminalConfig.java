package com.frontop.terminal.entity;

import lombok.Data;
import org.apache.logging.log4j.util.Strings;

/**
 * <p>
 * 终端配置
 * </p>
 *
 * @author :CCC
 * @since :2021-05-18 17:39:00
 **/

@Data
public class TerminalConfig {


    /**
     * 本机别名，用于服务器端更快定位
     */
    private String alias;


    /**
     * 服务器ip
     */
    private String ip;

    /**
     * 服务器地址
     */
    private String serverAddress;

    /**
     * 服务器授权码
     */
    private String authorizationCode;

    /**
     * 服务器端口
     */
    private Integer serverPort;

    /**
     * 发送心跳包间隔
     */
    private int heartbeatInterval;

    /**
     * 本地资源存放地址
     */
    private String resourceAddress;

    /**
     * 是否已配置过状态位: 0未;1已配置
     */
    private Boolean status;

    /**
     * 播放器重复播放：1列表循环，2单个循环,3播放完停止
     */
    private Integer playMode = 2;

    /**
     * 启动时播放器是否隐藏，true.隐藏，只有在播放视频时才显示，停止则隐藏
     */
    private boolean playerHide;

    /**
     * 启动时播放器是否全屏
     */
    private boolean playerFullScreen;

    /**
     * 启动时播放器设置窗口大小，格式1920*1080
     */
    private String playerSize;

    /**
     * 启动时播放器设置窗口窗口位置，格式 x y 100-100
     */
    private String playerLocation;

    /**
     * 播放器工具栏隐藏
     */
    private boolean playerToolbarHide;

    /**
     * 启动后浏览器是否隐藏，true隐藏
     */
    private boolean browserHide;

    /**
     * 启动后浏览器是否全屏 ，true全屏
     */
    private boolean browserFullScreen;

    /**
     * 打开播放器后是否自动播放视频,true自动播放
     */
    private boolean autoPlay;

    /**
     * 播放完顺序后，是否停留在最后一帧画面,true停留最后一帧
     */
    private boolean playerLastFrame;

    /**
     * 播放器多屏联动
     */
    private boolean playerGanged;

    /**
     * 本机socket地址
     */
    private String localSocketAddress;


    /**
     * 主屏socket地址
     */
    private String mainFrameSocketAddress;

    /**
     * 播放器多屏联动本机为主屏
     */
    private boolean playerGangedServer;


    /**
     * 播放器多屏联动本机为副屏
     */
    private boolean playerGangedClient;

    /**
     * 是否启用待机画面
     */
    private boolean enableStandby;

    /**
     * 版本号
     * 主版本号 . 子版本号 [ 修正版本号 [. 编译版本号 ]]
     * 示例 : 1.2.1
     */
    private String version = "1.1.6";

    public String getIp() {
        if (Strings.isNotBlank(this.getServerAddress())) {
            String url = this.serverAddress;
            ip = url.substring(url.indexOf("/") + 2, url.lastIndexOf(":"));
        }
        return ip;
    }
}
