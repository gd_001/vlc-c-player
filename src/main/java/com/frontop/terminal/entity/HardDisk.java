package com.frontop.terminal.entity;

import lombok.Data;

/**
 * <p>
 * 硬盘
 * </p>
 *
 * @author :CCC
 * @since :2021-05-18 11:50:00
 **/
@Data
public class HardDisk {

    /**
     * 盘符名称
     */
    private String name;

    /**
     * 盘路径
     */
    private String path;

    /**
     * 总空间
     */
    private String sumSpace;

    /**
     * 可用空间
     */
    private String usableSpace;

    /**
     * 可用空间比率
     */
    private String freeSpaceRate;

    @Override
    public String toString() {
        return "HardDisk{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", sumSpace='" + sumSpace + '\'' +
                ", usableSpace='" + usableSpace + '\'' +
                ", freeSpaceRate='" + freeSpaceRate + '\'' +
                '}';
    }
}
