package com.frontop.terminal.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 文件
 * </P>
 *
 * @author :CCC
 * @since :2021-06-08 10:59:00
 **/
@Data
public class File implements Serializable {

    private static final long serialVersionUID = -449208541669836202L;

    /**
     * id
     */
    private Long id;

    /**
     * 原文件名
     */
    private String name;

    /**
     * 文件后缀名
     */
    private String nameSuffix;

    /**
     * 文件的MD5签证
     */
    private String sign;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 文件下载状态:0未开始，1下载中，2下载完成
     */
    private Integer downloadStatus;

    /**
     * 文件标志状态:-1/删除，1/正常
     */
    private Integer statusFlag;

    /**
     * 文件类型
     */
    private String type;

    /**
     * 存放位置
     */
    private String locations;

    /**
     * 下载地址
     */
    private String url;


}
