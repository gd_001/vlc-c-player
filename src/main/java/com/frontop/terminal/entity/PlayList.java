package com.frontop.terminal.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 播放列表
 * </p>
 *
 * @author :CCC
 * @since :2021-06-08 11:27:00
 **/
@Data
public class PlayList implements Serializable {
    private static final long serialVersionUID = 704440063460165123L;


    /**
     * ID
     */
    private Long id;

    /**
     * 开启播放联动时的关联主屏资源id
     */
    private Long gangedId;

    /**
     * 资源名称
     */
    private String name;

    /**
     * 关联资源ID
     */
    private Long fileId;

    /**
     * 状态：0等待，1播放中
     */
    private Integer status = 0;

    /**
     * 文件存放位置
     */
    private String location;

    /**
     * 资源加入来源：0,中控服务器下载;1,手动导入
     */
    private Integer source = 0;

    /**
     * 类型：0普通资源;1,待机资源
     */
    private Integer type = 0;
}
