package com.frontop.terminal.entity;

import lombok.Data;

/**
 * <p>
 * 传感器
 * </p>
 *
 * @author :CCC
 * @since :2021-07-08 17:45:00
 **/
@Data
public class Sensors {
    /**
     * cpu温度
     */
    private String cpuTemperature;

    /**
     * cpu电压
     */
    private String cpuVoltage;

    /**
     * 风扇速度(多个)
     */
    private String fanSpeeds;

    @Override
    public String toString() {
        return "Sensors{" +
                "cpuTemperature='" + cpuTemperature + '\'' +
                ", cpuVoltage='" + cpuVoltage + '\'' +
                ", fanSpeeds='" + fanSpeeds + '\'' +
                '}';
    }
}
