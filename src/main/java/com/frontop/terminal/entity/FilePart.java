package com.frontop.terminal.entity;

import lombok.Data;

/**
 * <p>
 * 文件分片
 * </P>
 *
 * @author :CCC
 * @since :2021-06-25 15:39:00
 **/
@Data
public class FilePart {
    /**
     * 状态
     * 完成：true
     */
    public boolean status;

    /**
     * 分片id
     */
    private Integer id;

    /**
     * 文件名
     */
    private String name;

    /**
     * 文件存储位置
     */
    private String filePath;

    /**
     * FileId
     */
    private Long fileId;

    /**
     * 下载地址
     */
    private String url;

    /**
     * 数据开始位置
     */
    private Long start;

    /**
     * 数据结束位置
     */
    private Long end;
}
