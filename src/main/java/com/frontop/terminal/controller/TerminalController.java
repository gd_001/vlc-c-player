package com.frontop.terminal.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.frontop.terminal.entity.Message;
import com.frontop.terminal.entity.TerminalConfig;
import com.frontop.terminal.gangedsocket.core.PlayGangedServiceSocket;
import com.frontop.terminal.model.bo.TerminalCurrentFocusStatusBo;
import com.frontop.terminal.model.vo.RestResult;
import com.frontop.terminal.service.ICommandSerivce;
import com.frontop.terminal.util.MessageDisposeFactory;
import com.frontop.terminal.util.ReadDataUtil;
import com.frontop.terminal.util.TerminalApplictionContextUtil;
import com.frontop.terminal.vlcjplayer.VlcjPlayer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Objects;

/**
 * <p>
 * 终端控制器
 * </P>
 *
 * @author :CCC
 * @since :2012-01-01 00:31:00
 **/
@RequestMapping("/terminal")
@Controller
@RequiredArgsConstructor
public class TerminalController {

    @PostMapping("")
    @ResponseBody
    public RestResult distribute(@RequestBody Message message) {
        System.out.println("收到：" + message);
        if (StrUtil.isEmpty(message.getCommand())) {
            return RestResult.failed("command参数不能为空");
        }
        if (StrUtil.isEmpty(message.getType())) {
            return RestResult.failed("type参数不能为空");
        }

        ICommandSerivce iCommandSerivce = MessageDisposeFactory.getWorkers(message.getType());

        if (Objects.isNull(iCommandSerivce)) {
            return RestResult.failed("未找到'" + message.getType() + "'类型信息处理对象");
        }

        TerminalConfig terminalConfig = ReadDataUtil.getTerminalConfig();
        if (Objects.nonNull(terminalConfig)) {
            //开启了播放联动,并且是服务端
            if (terminalConfig.isPlayerGanged() && terminalConfig.isPlayerGangedServer()) {
                //通知播放联动集合
                PlayGangedServiceSocket.sendToAllSocket(JSON.toJSONString(message));
            }
        }


        return iCommandSerivce.executeWork(message);
    }

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    /**
     * 获取当前终端焦点任务
     *
     * @return true 当前焦点播放器 ；false 互动程序 ;无任务
     */
    @GetMapping("/getCurrentFocusTask")
    @ResponseBody
    public RestResult getCurrentFocusTask() {
        try {
            return RestResult.success(new TerminalCurrentFocusStatusBo(TerminalApplictionContextUtil.CURRENT_FOCUS_TASK
                    , TerminalApplictionContextUtil.RESOURCE_ID));
        } catch (Exception e) {
            return RestResult.failed("终端出现异常");
        }

    }


    @GetMapping("exit")
    @ResponseBody
    public void exit() {
        VlcjPlayer.exit();
        System.exit(0);
    }
}
