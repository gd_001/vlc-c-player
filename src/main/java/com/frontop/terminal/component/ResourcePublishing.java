package com.frontop.terminal.component;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import com.frontop.terminal.constant.PlayStatusConst;
import com.frontop.terminal.entity.PlayList;
import com.frontop.terminal.util.ReadDataUtil;
import com.frontop.terminal.util.TerminalApplictionContextUtil;
import com.frontop.terminal.util.WriteDataUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 资源发布
 * </p>
 *
 * @author :CCC
 * @since :2021-06-11
 **/
public class ResourcePublishing {

    public static String getResource() {
        String nextPath = null;
        //播放列表
        List<PlayList> playListList = ReadDataUtil.getPlayList();

        if (!Objects.requireNonNull(playListList).isEmpty()) {
            for (PlayList play : playListList) {
                //找出上一次在播放的记录接着播放 并且不为待机资源
                if (play.getStatus().equals(PlayStatusConst.UNDER_WAY) && !play.getType().equals(PlayStatusConst.TYPE_STANDBY_RESOURCE)) {
                    nextPath = play.getLocation();
                    //记录播放的资源id
                    TerminalApplictionContextUtil.RESOURCE_ID = play.getId() + "";
                    break;
                }
            }
        }

        nextPath = reset(nextPath, playListList);
        //更新文件状态
        WriteDataUtil.write(playListList, ReadDataUtil.PLAYLIST_LIST_CODING);

        return nextPath;
    }

    public static String nextResource() {
        String nextPath = null;
        //播放列表
        Map<Long, PlayList> playMap = ReadDataUtil.getPlayListMap();
        assert playMap != null;
        for (Map.Entry<Long, PlayList> longPlayListEntry : playMap.entrySet()) {
            PlayList play = longPlayListEntry.getValue();
            //如果播放过了更改状态为已播放
            if (play.getStatus().equals(PlayStatusConst.UNDER_WAY) && !play.getType().equals(PlayStatusConst.TYPE_STANDBY_RESOURCE)) {
                play.setStatus(PlayStatusConst.FINISH);

                continue;
            }

            //获取未播放的记录&&同时是已经下载完成的记录
            if (play.getStatus().equals(PlayStatusConst.WAITING)
                    && !StrUtil.isEmpty(play.getLocation())
                    && !play.getType().equals(PlayStatusConst.TYPE_STANDBY_RESOURCE)) {
                nextPath = play.getLocation();
                //更改播放状态
                play.setStatus(PlayStatusConst.UNDER_WAY);
                //记录播放的资源id
                TerminalApplictionContextUtil.RESOURCE_ID = play.getId() + "";
                break;
            }
        }

        //如果局部变量为null就重置状态位重复播放
        nextPath = reset(nextPath, new ArrayList<>(playMap.values()));


        //不为空保存
        if (!playMap.isEmpty()) {
            List<PlayList> playLists = new ArrayList<>(playMap.values());
            //保存
            WriteDataUtil.write(playLists, ReadDataUtil.PLAYLIST_LIST_CODING);
        }

        return nextPath;
    }

    public static String previousResource() {
        //记录上一个视频地址
        String previousPath = null;
        //记录准备播放的下标
        int index = 0;


        //播放列表
        List<PlayList> playLists = ReadDataUtil.getPlayList();

        if (Objects.nonNull(playLists) && !playLists.isEmpty()) {

            for (int i = 0; i < playLists.size(); i++) {
                PlayList play = playLists.get(i);
                //如果播放过了更改状态为已播放
                if (play.getStatus().equals(PlayStatusConst.UNDER_WAY) && !play.getType().equals(PlayStatusConst.TYPE_STANDBY_RESOURCE)) {
                    play.setStatus(PlayStatusConst.FINISH);
                    //记录下标
                    index = i;
                }
            }

            if (index == 0) {
                PlayList play = playLists.get(index);
                //只有一个视频资源那就继续播放
                previousPath = play.getLocation();
                //更改播放状态
                play.setStatus(PlayStatusConst.UNDER_WAY);
                //记录播放的资源id
                TerminalApplictionContextUtil.RESOURCE_ID = play.getId() + "";
            } else {
                for (int i = index; i > 0; i--) {
                    //获取上一个视频
                    PlayList play = playLists.get(index - 1);

                    //找出已经下载完的播放
                    if (!StrUtil.isEmpty(play.getLocation()) && !play.getType().equals(PlayStatusConst.TYPE_STANDBY_RESOURCE)) {
                        //更改播放状态
                        play.setStatus(PlayStatusConst.UNDER_WAY);
                        previousPath = play.getLocation();
                        //记录播放的资源id
                        TerminalApplictionContextUtil.RESOURCE_ID = play.getId() + "";
                    }
                }

                //index上面的元素不具备播放条件将重复播放之前的记录
                if (StrUtil.isEmpty(previousPath)) {
                    PlayList play = playLists.get(index);
                    //只有一个视频资源那就继续播放
                    previousPath = play.getLocation();
                    //更改播放状态
                    play.setStatus(PlayStatusConst.UNDER_WAY);
                    //记录播放的资源id
                    TerminalApplictionContextUtil.RESOURCE_ID = play.getId() + "";
                }
            }
        }

        //如果局部变量为null就重置状态位重复播放
        previousPath = reset(previousPath, playLists);


        //不为空保存
        if (!Objects.requireNonNull(playLists).isEmpty()) {
            //保存
            WriteDataUtil.write(playLists, ReadDataUtil.PLAYLIST_LIST_CODING);
        }

        return previousPath;
    }

    private static String reset(String nextPath, List<PlayList> playLists) {
        //如果局部变量为null就重置状态位重复播放
        if (StrUtil.isEmpty(nextPath)) {
            if (!playLists.isEmpty()) {
                for (PlayList p : playLists) {
                    //nextPath为空&&当前对象不为空时当前对象将播放
                    if (StrUtil.isEmpty(nextPath)
                            && !StrUtil.isEmpty(p.getLocation())
                            && !p.getType().equals(PlayStatusConst.TYPE_STANDBY_RESOURCE)) {
                        nextPath = p.getLocation();
                        p.setStatus(PlayStatusConst.UNDER_WAY);
                        //记录播放的资源id
                        TerminalApplictionContextUtil.RESOURCE_ID = p.getId() + "";
                        continue;
                    }

                    p.setStatus(PlayStatusConst.WAITING);
                }
            }
        }

        return nextPath;
    }

    /**
     * 播放列表空检测
     */
    public static String playListNullChke() {
        ThreadUtil.sleep(1000);

        //播放列表
        List<PlayList> playLists = ReadDataUtil.getPlayList();
        if (!(Objects.requireNonNull(playLists).isEmpty())) {

            boolean playStatus = true;
            String playPath = null;
            long playId = 0;
            //查找播放列表全部为待播放状态
            for (PlayList play : playLists) {
                if (!play.getStatus().equals(PlayStatusConst.WAITING) && !play.getType().equals(PlayStatusConst.TYPE_STANDBY_RESOURCE)) {
                    playStatus = false;
                }

                if (StrUtil.isEmpty(playPath) && !StrUtil.isEmpty(play.getLocation())) {
                    playPath = play.getLocation();
                    play.setStatus(PlayStatusConst.UNDER_WAY);
                    playId = play.getId();
                }
            }

            //找出下载成功的资源
            if (playStatus && !StrUtil.isEmpty(playPath)) {
                WriteDataUtil.write(playLists, ReadDataUtil.PLAYLIST_LIST_CODING);
                //记录播放的资源id
                TerminalApplictionContextUtil.RESOURCE_ID = playId + "";
                return playPath;
            }
        }
        return null;
    }

    /**
     * 获取待机资源
     *
     * @return 资源绝对路径
     */
    public static String getStandbyResource() {
        //播放列表
        List<PlayList> playLists = ReadDataUtil.getPlayList();
        if (Objects.nonNull(playLists) && !playLists.isEmpty()) {
            for (PlayList play : playLists) {
                if (play.getType().equals(PlayStatusConst.TYPE_STANDBY_RESOURCE) && !StrUtil.isEmpty(play.getLocation())) {
                    //记录播放的资源id
                    TerminalApplictionContextUtil.RESOURCE_ID = play.getId() + "";
                    return play.getLocation();
                }
            }
        }
        return null;
    }
}
