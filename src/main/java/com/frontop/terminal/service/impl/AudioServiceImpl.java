package com.frontop.terminal.service.impl;

import cn.hutool.core.util.StrUtil;
import com.frontop.terminal.constant.CommandConst;
import com.frontop.terminal.entity.Message;
import com.frontop.terminal.model.vo.RestResult;
import com.frontop.terminal.service.ICommandSerivce;
import com.frontop.terminal.vlcjplayer.view.main.ControlsPane;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * <p>
 * 播放器音量
 * </P>
 *
 * @author :CCC
 * @since :2021-06-16 16:58:00
 **/
@SuppressWarnings("AlibabaUndefineMagicConstant")
public class AudioServiceImpl implements ICommandSerivce {

    @Override
    public RestResult executeWork(Message message) {
        //如果具体命令为空不执行
        if (StrUtil.isEmpty(message.getCommand())) {
            return RestResult.failed("command 参数不能为空");
        }

        //获取当前音量
        int currentVolume = application().mediaPlayer().audio().volume();

        switch (message.getCommand()) {
            case CommandConst.AUDIO_MUTE:
                //静音
                application().mediaPlayer().audio().setMute(true);
                application().mediaPlayer().audio().setVolume(0);
                break;
            case CommandConst.AUDIO_CANCEL_MUTE:
                //取消静音
                application().mediaPlayer().audio().setMute(false);
                application().mediaPlayer().audio().setVolume(currentVolume);
                break;
            case CommandConst.AUDIO_GET_VOLUME:
                //获取音量 0-200范围
                return RestResult.success(currentVolume);
            case CommandConst.AUDIO_GET_MUTE:
                //是否被静音.如果是返回true，没有静音false
                boolean isMute = application().mediaPlayer().audio().isMute();
                return RestResult.success(isMute);
            case CommandConst.AUDIO_AMPLIFY:
                //扩大音量
                if (currentVolume <= 190) {
                    currentVolume = currentVolume + 10;
                    application().mediaPlayer().audio().setVolume(currentVolume);
                }
                ControlsPane.setVolumeSlider(currentVolume);
                System.out.println("增加音量，当前音量" + currentVolume);
                return RestResult.success(currentVolume);
            case CommandConst.AUDIO_DECREASE:
                //减少音量
                if (currentVolume >= 10) {
                    currentVolume = currentVolume - 10;
                    application().mediaPlayer().audio().setVolume(currentVolume);
                }
                ControlsPane.setVolumeSlider(currentVolume);
                System.out.println("减少音量，当前音量" + currentVolume);
                return RestResult.success(currentVolume);
            default:
                //都匹配不上

                int volume;
                try {
                    volume = Integer.parseInt(message.getCommand());
                } catch (NumberFormatException e) {
                    return RestResult.failed("未知的命令:" + message.getCommand());
                }

                //音量是否在规定的范围0-200
                if (volume < 0 || volume > 200) {
                    return RestResult.failed("参数不合法:" + message.getCommand());
                }

                application().mediaPlayer().audio().setVolume(volume);
        }
        return RestResult.success();
    }
}
