package com.frontop.terminal.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.frontop.terminal.cmd.AbstractCmdCommand;
import com.frontop.terminal.component.ResourcePublishing;
import com.frontop.terminal.constant.CommandConst;
import com.frontop.terminal.constant.CommonNumBerCodConst;
import com.frontop.terminal.entity.File;
import com.frontop.terminal.entity.Message;
import com.frontop.terminal.entity.PlayList;
import com.frontop.terminal.entity.Program;
import com.frontop.terminal.entity.TerminalConfig;
import com.frontop.terminal.model.vo.RestResult;
import com.frontop.terminal.service.ICommandSerivce;
import com.frontop.terminal.util.FileUtils;
import com.frontop.terminal.util.ReadDataUtil;
import com.frontop.terminal.util.TerminalApplictionContextUtil;
import com.frontop.terminal.vlcjplayer.VlcjPlayer;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * <p>
 * 播放类型业务
 * </P>
 *
 * @author :CCC
 * @since :2021-06-16
 **/
@Slf4j
public class PlayServiceImpl implements ICommandSerivce {

    @Override
    public RestResult executeWork(Message message) {
        //如果具体命令为空不执行
        if (StrUtil.isEmpty(message.getCommand())) {
            return RestResult.failed("command 参数不能为空");
        }

        TerminalConfig terminalConfig = Objects.requireNonNull(ReadDataUtil.getTerminalConfig());
        boolean playerHide = terminalConfig.isPlayerHide();
        boolean playerFullScreen = terminalConfig.isPlayerFullScreen();

        switch (message.getCommand()) {
            case CommandConst.PLAY:
                //播放
                application().mediaPlayer().controls().play();
                //播放器隐藏时在播放的时候要显示
                if (playerHide) {
                    VlcjPlayer.vlcPlayerVisible(true);
                }
                if (playerFullScreen) {
                    //全屏
                    VlcjPlayer.vlcPlayerFullScreen(true);
                }
                break;
            case CommandConst.PLAY_PAUSE:
                //暂停
                application().mediaPlayer().controls().setPause(true);
                break;
            case CommandConst.PLAY_STOP:
                //停止
                application().mediaPlayer().controls().stop();

                //开机隐藏开启了的话在停止时隐藏播放器
                if (playerHide) {
                    VlcjPlayer.vlcPlayerVisible(false);
                }

                break;
            case CommandConst.PLAY_LAST:
                //上一个
                String previous = ResourcePublishing.previousResource();
                if (!StrUtil.isEmpty(previous)) {
                    VlcjPlayer.play(previous);
                }
                break;
            case CommandConst.PLAY_NEXT:
                //下一个
                String next = ResourcePublishing.nextResource();
                if (!StrUtil.isEmpty(next)) {
                    VlcjPlayer.play(next);
                }
                break;
            case CommandConst.PLAY_NEXT_FRAME:
                //快进
                application().mediaPlayer().controls().skipTime(5000);
                break;
            case CommandConst.PLAY_UP_FRAME:
                //后退
                application().mediaPlayer().controls().skipTime(-5000);
                break;
            case CommandConst.PLAY_REBROADCAST:
                //重播
                application().mediaPlayer().controls().setTime(0);
                //播放
                application().mediaPlayer().controls().play();
                break;
            default:
                //都匹配不上默认为播放资源

                //如果是播放列表id可以直接转换
                long playId;
                try {
                    playId = Long.parseLong(message.getCommand());
                } catch (NumberFormatException e) {
                    log.warn("未知的命令:{}", message.getCommand());
                    return RestResult.failed("未知的命令:" + message.getCommand());
                }

                PlayList play = Objects.requireNonNull(ReadDataUtil.getPlayListMap()).get(playId);
                if (Objects.isNull(play)) {
                    log.warn("播放资源不存在");
                    return RestResult.failed("播放资源不存在");
                }

                //如果是中控系统发布的资源
                if (play.getSource().equals(0)) {
                    //拿出文件记录，检查是否多媒体类型
                    File media = Objects.requireNonNull(ReadDataUtil.getFileMap()).get(play.getFileId());
                    if (Objects.isNull(media)) {
                        log.warn("数据源不存在");
                        return RestResult.failed("数据源不存在");
                    }

                    //如果不是多媒体类型
                    if (!FileUtils.isMultimedia(media.getNameSuffix())) {
                        log.warn("不支持非多媒体类型播放");
                        return RestResult.failed("不支持非多媒体类型播放");
                    }

                    if (StrUtil.isEmpty(play.getLocation())) {
                        log.warn("资源未下载完成,需等下载完才可以播放");
                        return RestResult.failed("资源未下载完成,需等下载完才可以播放");
                    }
                } else {
                    String suffix = FileUtil.getSuffix(play.getLocation());
                    //如果不是多媒体类型
                    if (!FileUtils.isMultimedia(suffix)) {
                        log.warn("不支持非多媒体类型播放");
                        return RestResult.failed("不支持非多媒体类型播放");
                    }
                }

                startPlay(playId, play);
        }
        return RestResult.success();
    }

    private void startPlay(long playId, PlayList play) {
        //上次切换到互动程序时播放器已经隐藏了的话，播放资源时再把播放器显示 || 开机隐藏
        if (TerminalApplictionContextUtil.CURRENT_FOCUS_TASK.equals(CommonNumBerCodConst.TWO)) {
            //读取程序列表
            Program program = Objects.requireNonNull(ReadDataUtil.getProgramListMap()).get(TerminalApplictionContextUtil.RESOURCE_ID);
            if (Objects.nonNull(program)) {
                //获取应用程序名称
                String programName = program.getLocation().substring(program.getLocation().lastIndexOf("\\") + 1);

                System.out.println("切换播放器前尝试关闭互动程序进程:[" + programName + "]");
                //关闭互动程序
                AbstractCmdCommand cmdCommand = AbstractCmdCommand.getCmdCommandFactory();
                cmdCommand.killedExe(programName);
            }
        } else if (TerminalApplictionContextUtil.CURRENT_FOCUS_TASK.equals(CommonNumBerCodConst.ZERO)) {
            //TerminalApplictionContextUtil.CURRENT_FOCUS_TASK 0的话意味着程序刚启动，同时播放器隐藏了，这个时候不管互动程序启动了没都要试着关闭
            List<Program> programList = ReadDataUtil.getProgramList();
            if (Objects.nonNull(programList) && !programList.isEmpty()) {
                Program program = programList.get(0);
                if (!ObjectUtil.isEmpty(program)) {
                    //获取应用程序名称
                    String programName = program.getLocation().substring(program.getLocation().lastIndexOf("\\") + 1);

                    System.out.println("首次切换播放器前尝试关闭互动程序进程:[" + programName + "]");
                    //关闭互动程序
                    AbstractCmdCommand cmdCommand = AbstractCmdCommand.getCmdCommandFactory();
                    cmdCommand.killedExe(programName);
                }
            }
        }

        TerminalConfig terminalConfig = ReadDataUtil.getTerminalConfig();

        assert terminalConfig != null;
        if (terminalConfig.isPlayerHide()) {
            //隐藏播放器选项启用的话在播放视频时显示播放器
            VlcjPlayer.vlcPlayerVisible(true);
        }
        if (terminalConfig.isPlayerFullScreen()) {
            //全屏
            VlcjPlayer.vlcPlayerFullScreen(true);
        }

        //播放
        VlcjPlayer.play(play.getLocation());

        //记录播放的资源id
        TerminalApplictionContextUtil.RESOURCE_ID = playId + "";
    }
}
