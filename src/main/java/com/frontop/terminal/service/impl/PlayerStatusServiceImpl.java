package com.frontop.terminal.service.impl;

import cn.hutool.core.util.StrUtil;
import com.frontop.terminal.constant.CommandConst;
import com.frontop.terminal.entity.Message;
import com.frontop.terminal.model.vo.RestResult;
import com.frontop.terminal.service.ICommandSerivce;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * <p>
 * 播放器状态
 * </P>
 *
 * @author :CCC
 * @since :2021-06-16
 **/
public class PlayerStatusServiceImpl implements ICommandSerivce {

    @Override
    public RestResult executeWork(Message message) {
        //如果具体命令为空不执行
        if (StrUtil.isEmpty(message.getCommand())) {
            return RestResult.failed("command 参数不能为空");
        }


        switch (message.getCommand()) {
            case CommandConst.PLAYER_STATUS_IS_PLAY:
                //当前播放器是否在播放资源
                boolean playStatus = application().mediaPlayer().status().isPlaying();
                return RestResult.success(playStatus);

            default:
                return RestResult.failed("未知的命令:" + message.getCommand());
        }
    }

}
