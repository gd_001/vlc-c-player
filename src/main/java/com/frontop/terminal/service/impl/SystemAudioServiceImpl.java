package com.frontop.terminal.service.impl;

import cn.hutool.core.util.StrUtil;
import com.frontop.terminal.constant.CommandConst;
import com.frontop.terminal.entity.Message;
import com.frontop.terminal.model.vo.RestResult;
import com.frontop.terminal.service.ICommandSerivce;
import com.frontop.terminal.util.SystemVolumeUtils;

/**
 * <p>
 * 系统音量
 * </P>
 *
 * @author :CCC
 * @since :2021-06-16 16:58:00
 **/
@SuppressWarnings("AlibabaUndefineMagicConstant")
public class SystemAudioServiceImpl implements ICommandSerivce {

    @Override
    public RestResult executeWork(Message message) {
        //如果具体命令为空不执行
        if (StrUtil.isEmpty(message.getCommand())) {
            return RestResult.failed("command 参数不能为空");
        }

        switch (message.getCommand()) {
            case CommandConst.SYSTEM_AUDIO_MUTE_ON_OFF:
                //静音
                SystemVolumeUtils.controlSystemVolume(0);
                break;
            case CommandConst.SYSTEM_AUDIO_AMPLIFY:
                //增加音量
                SystemVolumeUtils.controlSystemVolume(1);
                break;
            case CommandConst.SYSTEM_AUDIO_DECREASE:
                //减少音量
                SystemVolumeUtils.controlSystemVolume(2);
                break;
            default:
                //都匹配不上
                return RestResult.failed("未知的命令:" + message.getCommand());
        }
        return RestResult.success();
    }
}
