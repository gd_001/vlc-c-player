package com.frontop.terminal.service.impl;

import com.frontop.terminal.constant.CommandConst;
import com.frontop.terminal.entity.Message;
import com.frontop.terminal.model.vo.RestResult;
import com.frontop.terminal.service.ICommandSerivce;
import uk.co.caprica.vlcj.player.base.MarqueePosition;

import static com.frontop.terminal.vlcjplayer.Application.application;

/**
 * <p>
 * 中控系统授权许可
 * </p>
 *
 * @author Frontop
 * @since :2022-08-12
 **/
public class LicenseServiceImpl implements ICommandSerivce {
    /**
     * 执行业务
     *
     * @param message 消息对象
     * @return T
     */
    @Override
    public RestResult executeWork(Message message) {
        //打印字幕未激活信息
        if (message.getCommand().equals(CommandConst.LICENSE_UNACTIVATED)) {
            application().mediaPlayer().marquee().setText("中控系统未激活,请联系工作人员进行激活.\r\nThe trial period of the program is over, please purchase the genuine copy.");
            application().mediaPlayer().marquee().enable(true);
            application().mediaPlayer().marquee().setOpacity(127);
            application().mediaPlayer().marquee().setPosition(MarqueePosition.CENTRE);
        } else if (message.getCommand().equals(CommandConst.LICENSE_ACTIVATED)) {
            //关闭字幕
            application().mediaPlayer().marquee().enable(false);
        } else {
            return RestResult.failed("未知指令");
        }
        return RestResult.success();
    }
}
