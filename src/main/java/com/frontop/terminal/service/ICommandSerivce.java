package com.frontop.terminal.service;

import com.frontop.terminal.entity.Message;
import com.frontop.terminal.model.vo.RestResult;

/**
 * <p>
 * 命令业务接口
 * </p>
 *
 * @author :CCC
 * @since :2021-05-26 14:12:00
 **/
public interface ICommandSerivce {

    /**
     * 执行业务
     *
     * @param message 消息对象
     * @return T
     */
    RestResult executeWork(Message message);
}
